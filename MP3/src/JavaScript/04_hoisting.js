// console.log(sum(1, 41)); // 42

// function sum(a,b) {
//     return a + b;
// }

// -------
// console.log(i); // undefined
// var i = 42;

// ------- const, let
// console.log(num); // Reference error
// const num = 42;
// console.log(num); // 42

// ---- func expression and function declaration
// console.log(square(25)); // 625
// function square(num) { // function declaration
//     return num ** 2;
// }

// console.log(square(25)); // Reference error
// const square = function(num) { // function expressions
//     return num ** 2;
// }