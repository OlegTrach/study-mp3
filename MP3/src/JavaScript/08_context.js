// const person = {
//     surname: 'Stark',
//     knows: function(what, name) {
//         console.log(`You know ${what}, ${name} ${this.surname}`);
//     }
// }

// const john = { surname: 'Snow' };

// person.knows('all', 'Bran');
// person.knows.call(john, 'knows nothing', 'John');
// person.knows.apply(john, ['knows nothing', 'John']);
// person.knows.bind(john, 'knows nothing', 'John')();


// ------- functions

// function Person(name, age) {
//     this.name = name;
//     this.age = age;

//     console.log(this);
// }

// const Helena = new Person('Helena', 21);

// Explicit binding
// function logThis() {
//     console.log(this);
// }

// const obj = {
//     num: 42
// };

// logThis.apply(obj);
// logThis.call(obj);
// logThis.bind(obj)();

// // Implicit binding
// const animal = {
//     legs: 4,
//     logThis: function() {
//         console.log(this);
//     }
// }

// animal.logThis();

// --------- Arrow function

function Cat(color) {
    this.color = color;
    console.log('This', this);
   (() => console.log('Arrow this', this))(); // Doesn't create own context
}

new Cat('red');