function Cat(color, name) {
    this.color = color;
    this.name = name;
}

// const cat = new Cat('black', 'CAT');
// console.log(cat)

function myNew(constructor, ...args) { // Our owns new
    const obj = {};
    Object.setPrototypeOf(obj, constructor.prototype);
    return constructor.apply(obj, args) || obj;
}

const cat = myNew(Cat, 'black', 'CAT');
console.log(cat);