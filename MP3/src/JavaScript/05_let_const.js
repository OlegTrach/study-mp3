// Let
// let a = 'Variable a';
// let b = 'Variable b';

// {
//     a = 'New variable a';
//     let b = 'local variable b';
//     console.log('Scope A', a); // Scope A New variable a
//     console.log('Scope B', b); // Scope B local variable b
// }

// console.log('A', a); // A new variable a
// console.log('B', b); // B Variable b

// Const
// const PORT = 8080;
// PORT = 2000; // Error assignment to constant variable

// we can modify Arrays and Objects
const array = ['JavaScript', 'is', 'Awesome'];
array.push('!');
array[0] = 'JS';
console.log(array); // ['JavaScript', 'is', 'Awesome', '!']

// array = ''; // Error assignment to constant variable