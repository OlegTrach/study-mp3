// function sayHello(name) {
//     const message = `Hello ${name}!`;

//     return function() {
//         console.log(message);
//     }
// }

// const helloToElena = sayHelloTo('Elena');
// const helloToIhor = sayHelloTo('Igor');
// helloToElena();// Hello Elena!
// helloToIgor(); // Hello Igor!

//--------------- in functions
// function createFrameworkManager() {
//     const frameworks = [ 'Angular', 'React' ];

//     return {
//         print: function() {
//             console.log(frameworks);
//         },
//         add: function(newFramework) {
//             frameworks.push(newFramework);
//         }
//     }
// }

// const manager = createFrameworkManager();
// manager.print();

// manager.add('Vue');
// manager.print();

// ----- setTimeout()

const fibonachi = [1, 2, 3, 5, 8, 13];

for (var i = 0; i < fibonachi.length; i ++) {
    (function(j) {
        setTimeout(function() {
            console.log(`fibonachi[${j}] = ${fibonachi[j]}`);
        }, 1500);
    })(i)
}
