// null
// undefined
// boolean
// string
// number
// object
// symbols

// console.log(typeof null); // object
// console.log(typeof undefined); // undefined
// console.log(typeof true); // boolean
// console.log(typeof ''); // string
// console.log(typeof 5); // number
// console.log(typeof Object()); // object
// console.log(typeof Symbol('Js')); // symbol
// console.log(typeof function() {}); // function not type
// console.log(typeof NaN); // number

// TYPE CONVERSION

// falsy values
// '', 0, null, undefined, NaN, false
// console.log(Boolean(0)); // false
// console.log(Boolean('')); // false
// console.log(Boolean(' ')); // true

// String and numbers

// console.log(1 + '2'); // string 12
// console.log('' + 1 + 0); // string 10
// // JS doesn't have - for string
// console.log('' - 1 + 0 ); // number -1
// // also JS cant multiply two strings so it convert them to numbers
// console.log('3' * '8'); // number 24
// console.log(4 + 10 + 'px'); //string 14px
// console.log('px' + 5 + 3); // string px53
// console.log('42' - 40); // number 2
// console.log('42px' - 5); // number NaN
// console.log(null + 2); // number 2 since null converts to 0 here
// console.log(undefined + 42); // number NaN

// == vs ===

// console.log(2 == '2'); // true
// console.log(2 === '2'); // false
// console.log(undefined == null); // true
// console.log(undefined === null); // false
// console.log('0' == false); // true
// console.log('0' == 0); // true

// =====
// console.log(false == ''); // true
// console.log(false == []); // true
// console.log(false == {}); // false
// console.log('' == 0); // true
// console.log('' == []); // true
// console.log('' == {}); // false
// console.log(0 == []); // true
// console.log(0 == {}); // false
// console.log(0 == null); // false

