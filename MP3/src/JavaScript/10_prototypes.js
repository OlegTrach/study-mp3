// __proto__ refs to prototype object (to parent object)
// Object.getPrototypeOf

// function Cat(name, color) {
//     this.name = name;
//     this.color = color;
// }

// Cat.prototype.voice = function() {
//     console.log(`Cat ${this.name} says myay`);
// }

// const cat = new Cat('CAT', 'white');
// console.log(Cat.prototype);
// console.log(cat);
// console.log(cat.__proto__ === Cat.prototype);
// console.log(cat.constructor);
// cat.voice();

// ------------------- prototype chain
function Person() {};
Person.prototype.legs = 2;
Person.prototype.skin = 'white';

const person = new Person();
person.name = 'Oleh';

console.log(person.legs);
console.log('name' in person);
console.log('skin' in person);

console.log(person.hasOwnProperty('name'));
console.log(person.hasOwnProperty('skin'));

// ------- Object.create
let proto = { year: 2021 };
const myYear = Object.create(proto);

// proto.year = 2020; // change in object as well
proto = { year: 2015 }; // change nothing

console.log(myYear.year);
console.log(myYear.hasOwnProperty('year'));
console.log(myYear.__proto__ === proto);