// PATTERNS

// ***CONSTRUCTOR***
function Person(name, age, country) {
    this.name = name;
    this.age = age;
    this.country = country;
}

const person1 = new Person("John", 23, "UA");
// console.log("Person", person1);

// ***SINGLETON***
var counterModule = (function () {
    var counter,
        instance;

    var getCounter = function () {
        return counter;
    }

    var increaseCounter = function () {
        counter++;
    }

    var createInstance = function () {
        return {
                getCounter: getCounter,
                increaseCounter: increaseCounter
            }
        }

    return {
        getInstance: function () {
            return instance || (instance = createInstance());
        }
    }
}());

// console.log("counterModule", counterModule.getInstance());

// ***FACTORY***
class FullTime {
    constructor() {
        this.rate = "$12"
    }
}

class PartTime {
    constructor() {
        this.rate = "$11"
    }
}

class Temporary {
    constructor() {
        this.rate = "$10"
    }
}

class Contractor {
    constructor() {
        this.rate = "$15"
    }
}

class Employee {
    create(type) {
        let employee;
        if (type == "fulltime"){
            employee = new FullTime() 
        } else if (type === "parttime") {
            employee = new PartTime()
        } else if ( type  === "temporary") {
            employee = new Temporary()
        } else if ( type  === "contractor") {
        employee = new Contractor()
        }
        employee.type = type;
        employee.say = function() {
            console.log(`${this.type}: rate ${this.rate}/hour`)
        }
        return employee
    }
}

const factory = new Employee();
fulltime = factory.create("fulltime");
parttime = factory.create("parttime");
temporary = factory.create("temporary");
contractor = factory.create("contractor");

// fulltime.say();
// parttime.say();
// temporary.say();
// contractor.say();

//  ***MODULE***
var counterModule = (function(){
    var counter = 0;

    var increaseCounter = function() {
        counter++;
    }

    var getCounter = function() {
        return counter;
    }

    return {
        getCounter: getCounter,
        increaseCounter: increaseCounter
    }
}())


//  *****OBSERVER******

function Observable() {
    const observers = [];

    this.sendMessage = function (msg) {
        for (let i = 0; i < observers.length; i++) {
            observers[i].notify(msg);
        }
    };

    this.addObserver = function(observer) {
        observers.push(observer);
    };
}

function Observer(behavior) {
    this.notify = function(msg) {
        behavior(msg);
    }
}

const observable = new Observable();
const obs1 = new Observer(msg => console.log(msg));
const obs2 = new Observer(msg => console.log(msg));
observable.addObserver(obs1);
observable.addObserver(obs2);
setTimeout(() => {observable.sendMessage("Hello from Observer")}, 3000);

//      *****OBSERVER2*****

function Click() {
    this.handlers = [];
}

Click.prototype = {
    subscribe: function (fn) {
        this.handlers.push(fn);
    },

    unsubscribe: function (fn) {
        this.handlers.filter(handler => {
            if (fn !== handler) {
                return handler;
            }
        })
    },

    fire: function (o, thisObj) {
        let scope = thisObj;
        this.handlers.forEach(handler => {
            handler.call(scope, o);
        });
    }
}

let click = new Click();

click.subscribe(() => {
    console.log('clicked');
});

click.fire();

// counterModule.increaseCounter();
// counterModule.increaseCounter();
// counterModule.increaseCounter();
// console.log(counterModule.getCounter());
