// The candidate has a basic understanding of developing apps using React, can't clearly 
// explain how `setState` works. Also failed most of practical task of common problems 
// (componentDidMount vs componentWillMount, Portals, etc...)

// - Search Insert Position - candidate was not able to present final solution, 
//     only talk thought it (but with a few hints) A few books I suggest to read:
// - You Don't Know JS: Scope & Closures: https://github.com/getify/You-Dont-Know-JS/blob/master/scope%20&%20closures/README.md#you-dont-know-js-scope--closures
// - You Don't Know JS: this & Object Prototypes: https://github.com/getify/You-Dont-Know-JS/tree/master/this%20%26%20object%20prototypes

// EventLoop:
// - https://youtu.be/cCOL7MC4Pl0
// - https://youtu.be/u1kqx6AenYw

// Few home tasks:
// - https://leetcode.com/problems/search-insert-position/
// - https://leetcode.com/problems/single-number/description
// - https://leetcode.com/problems/find-the-duplicate-number/description

// The candidate has very basic understanding of javascript core. Has small knowledge of 
// context ( did not solve tasks ). 
// Was able to explain event loop but was not able to solve practical example 
// ( looks like he just learned it by rote ). 
// Has good understanding of prototypal inheritance ( solved task ).
// Has problems with explaining React main concepts even though he has been working with 
// it for about 7 months. 
// Was not able to solve and explain any React related tasks. 
// He answered the most questions with hasitating and many questions 
// he guessed without clear understanding why it works that way.

// REACT

// - React inheritance
// - HOC/FOC, 
// - Functional components lifecycle hooks
// - Life cycle hooks - types
// - Як змінюється стейт в компоненті під капотом
// - Чи можна змінити стейт чимось іншим ніж setState
// - PropTypes react vs TS typings (static and runtime)
// - Юзкейси для використання ref та props (порушення диклеративної концепції)
// - Controlled та Uncontolled components. - чи можуть бути всі чи не всі, які краще, 
//   прнципова різниця, і тд. Input type file, чи можемо міняти тип поведінки в одному 
//   і тому ж компоненті
// - One/two way data binding
// - Стейт змінити в рендері
// - Сет стейт синхроннний, але може бути асинхронний
// - get derived state from props