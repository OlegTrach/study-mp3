// How length works
var words = ["hello"];
words[6] = "welcome";
console.log(words.length);

// Write function for generating array
// function generateArray(n, a) {
//     var arr = [];
//     for (var i = 0; i < n; i++) {
//         arr.push(a);
//     }
//     console.log(arr);
// };
function generateArray(n, a) {
    var arr = Array.apply(null, Array(n));
    return arr.map((x) => x = a);
    // OR
    // return Array(n).fill(a);
}

// var arr = Array.apply(null, Array(n)); create array with 5 undefined items and has length = 5
// var arr = Array(n); create array [empty x 5] with 5 empty values but has length = 5

console.log(generateArray(5, 0));
console.log(generateArray(3, 2));

// Write function for adding numbers
function addNum(a){
    var sum = 0;

    function closure(el) {
        if (el) {
            sum += el;
            return closure;
        } else {
            return sum;
        }
    };

    return closure(a); 
};

console.log(addNum(1)(2)(3)(4)());
console.log(addNum(1)(1)(1)(1)(1)());

// Validate if string contains only 3 characters
function validateString(str) {
    const regExp = /^[a-z]{3}$/;
    return console.log(regExp.test(str));
}

validateString('abc');   // true
validateString('abcd');  // false
validateString('a2c');   // false

// Remove all non numeric elements from array using filter and regexp
// [0, , false, 1, ,'', undefined, , 2, , null, , 3, 'asd']  --> [ 0, 1, 2, 3 ]"

const wholeArray = [0, , false, 1, ,'', undefined, , 2, , null, , 3, 'asd'];
// const regex = /^[0-9]+$/;
const regex = /^\d+$/;

console.log(wholeArray.filter((el) => regex.test(el)));
