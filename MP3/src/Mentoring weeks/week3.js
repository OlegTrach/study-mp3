const array = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

// first
console.log("**************foreEach******************");
array.forEach((e) => console.log(e));

// second
console.log("**************for***************");
for(let i = 0; i < array.length; i++) {
    console.log(array[i]);
};

// third
console.log("**************for-in******************");
for (const key in array) {
    if (array.hasOwnProperty(key) &&
        /^0$|^[1-9]\d*$/.test(key) &&
        key <= 4294967294) {
        console.log(array[key]);
    }
}

// fourth only for arrays
console.log("**************for-of******************");
for (const val of array) {
    console.log(val);
}

// fifth
console.log("*****Use an iterator explicitly*****");
let iterator = array.entries();
let entry;
while (!(entry = iterator.next()).done) {
    console.log(entry.value[1]);
}


// let const
{
    let _nested = 'secret'
    function nested () {
      return _nested
    }
  }
  console.log(nested())

//   let b;
//   console.log(b);
  let b = "b";

//   var a;
  console.log(a);
  var a="a";
  
//   *********Homework************
// 1. Переписати функцію generateArray через Array.from()
function newGenerateArray(a, b) {
    return Array.from(Array(a), () => b);
}

console.log(newGenerateArray(5, 0));
console.log(newGenerateArray(3, 2));
// 2. Array(7) vs Array.of(7)
Array.of(3);       // [3] 
Array.of(1, 2, 3); // [1, 2, 3]

Array(3);          // [ , , ]
Array(1, 2, 3);    // [1, 2, 3]
// 3. Написати функцію isArray(), яка перевірить, чи на вхід прийшов масив чи ні
function isArray(instance) {
    // - Перевірка через стрінгу '[object Array]'
    // return Object.prototype.toString.call(instance) === '[object Array]';
    // - Зробити через нативний метод масиву
    return instance instanceof Array;
}

console.log(isArray({}));
console.log(isArray([1, 2, 3]));

// - Переписати цю функцію у вигляді поліфіла для масива
Array.prototype.myIsArray = function(instance) {
    return Object.prototype.toString.call(instance) === '[object Array]';
}

console.log(Array.prototype.myIsArray([1, 2]));
console.log(Array.prototype.myIsArray({}));

// 4. Написати поліфіл для отримння першого елементу масива, e.g. arr.first();

Array.prototype.first = function() {
    if(this.length > 0) {
        return this[0];
    } else {
        return "This array is empty"
    }
}

console.log([1, 2, 3, 4, 5].first());
console.log([].first());

// 5. Оператори (+/- etc.) та пріоритет виконання
// 6. Інкремент/декремент, постфікс/інфікс
// 7. Логічні оператори &&, || 
// 8. Цикли for ... in та for .. of
// for ... in ітерує по властивостях names не valueсах в обєкті

let array1 = [22, 33, 44];
array1.foo = "hello";
 
for (let i in array1) {
   console.log(i); // "0", "1", "2", "foo"
}

// for .. of ітерує по ітереційних обєктах (не застосовється для обєктів!!!)
let array2 = [22, 33, 44];
array2.foo = "hello";
 
for (let i of array2) {
   console.log(i); // "22", "33", "44"
}



