// WEEK 8
// *****ECMAScript Advanced*****

// Promises
// Know how [Promise] works
   /*  function httpGet(url) {

        return new Promise(function (resolve, reject) {

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onload = function () {
                if (this.status == 200) {
                    resolve(this.response);
                } else {
                    var error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            };

            xhr.onerror = function () {
                reject(new Error("Network Error"));
            };

            xhr.send();
        });
    } */
// Know promise chain pattern

// Be able to compare promise and callback patterns

// Be able to handle errors in promises
// Be able to use promisification pattern
// Iterators
// Know [Iterator] interface
// Be able to create custom iterator
    // DOM nodes
    /* Iterable objects is a generalization of arrays. That’s a concept that allows 
    to make any object useable in a for..of loop. */

    /* Objects that can be used in for..of are called iterable.

    Technically, iterables must implement the method named Symbol.iterator.
        The result of obj[Symbol.iterator] is called an iterator. It handles the further 
        iteration process.
        An iterator must have the method named next() that returns an object {done: Boolean, 
            value: any}, here done:true denotes the iteration end, otherwise the value is the 
            next value.
    The Symbol.iterator method is called automatically by for..of, but we also can do it directly.
    Built-in iterables like strings or arrays, also implement Symbol.iterator.
    String iterator knows about surrogate pairs.

    Objects that have indexed properties and length are called array-like. Such objects may also have 
    other properties and methods, but lack the built-in methods of arrays.

    If we look inside the specification – we’ll see that most built-in methods assume that they work 
    with iterables or array-likes instead of “real” arrays, because that’s more abstract.

    Array.from(obj[, mapFn, thisArg]) makes a real Array of an iterable or array-like obj, and we can 
    then use array methods on it. The optional arguments mapFn and thisArg allow us to apply a function 
    to each item. */
    
    // example

    /* let range = {
        from: 1,
        to: 5
    };

    // 1. call to for..of initially calls this
    range[Symbol.iterator] = function () {

        // ...it returns the iterator object:
        // 2. Onward, for..of works only with this iterator, asking it for next values
        return {
            current: this.from,
            last: this.to,

            // 3. next() is called on each iteration by the for..of loop
            next() {
                // 4. it should return the value as an object {done:.., value :...}
                if (this.current <= this.last) {
                    return { done: false, value: this.current++ };
                } else {
                    return { done: true };
                }
            }
        };
    };

    // now it works!
    for (let num of range) {
        console.log(num); // 1, then 2, 3, 4, 5
    } */


// Generators
    /* Генераторы – новый вид функций в современном JavaScript. 
    Они отличаются от обычных тем, что могут приостанавливать своё выполнение, 
    возвращать промежуточный результат и далее возобновлять его позже, в произвольный 
    момент времени. */

    /*function* generateSequence() {
            yield 1;
            yield 2;
            return 3;
        } */
// Know generator syntax
    /*     function* generateSequence() {
            yield 1;
            yield 2;
            return 3;
        }

        let res = generateSequence();

        console.log(res.next());
        console.log(res.next());
        console.log(res.next()); */
// Be able to compare generator and iterator
    /* по наличию метода next(), генератор связан с итераторами. В частности, он 
    является итерируемым объектом. Его можно перебирать и через for..of: */
    /* function* generateSequence() {
        yield 1;
        yield 2;
        return 3;
    }

    let generator = generateSequence();

    for (let value of generator) {
        alert(value); // 1, затем 2
    } */
// Understand how [yield] works
// Understand plain async code
// Be able to handle errors in generators
    /* function* gen() {
        try {
        // в этой строке возникнет ошибка
        let result = yield "Сколько будет 2 + 2?"; // (**)
    
        alert("выше будет исключение ^^^");
        } catch(e) {
        alert(e); // выведет ошибку
        }
    }
    
    let generator = gen();
    
    let question = generator.next().value;
    
    generator.throw(new Error("ответ не найден в моей базе данных")); // (*) */

/* //Class hoisting
 function Hobbit() {
    console.log("Hobbit");
 }

const frodo = new Hobbit();
frodo.height = 100;
frodo.weight = 300;

console.log(frodo);

class Hobbit {
    constructor(height, weight) {
        this.height = height;
        this.weight = weight;
    }
}
*/

// The same code with promise and with async/await

// with promices
function requestBook(id) {
    return bookAPIHelper.getBook(id)
    .then(book => {console.log(book)});
}

// async/await 
async function requestBookByAwait(id) {
    const book = await bookAPIHelper.getBook(id);
    console.log(book);
}
