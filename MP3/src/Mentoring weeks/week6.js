
// Function default parameters
// Discover default parameters concept and limitations
// Spread operator for Function
// Know how to use spread operator for Function arguments
// Be able to compare [arguments] and spread operator
// Spread operator for Array
// Understand and able to use spread operator for Array concatenation
// Destruction
// Be able to discover destruction concept
// Understand variables and Function arguments destruction
// String templates
// Know String template syntax and rules
// [for..of] loop
// Know how [for..of] loop works
// Be able to compare [for..of] loop with other types of loops

/* function greet() {
    if(greet.locale === "ua") {
        console.log("Привіт!");
    } else if (greet.locale === "es") {
        console.log("Hola!");
    } else {
        console.log("Hello!");
        console.dir(arguments.callee.name);
        console.dir(arguments.callee.caller.name);
        
    }
}

greet();
greet.locale = "ua";
greet(); */

/* let arr1 = ["a", "b", "c"];

if(~arr1.indexOf("b")) {
    console.log("element included");
} else {
    console.log("element not included");
} */

// Implement function for basic check instance
  /* function instanceOf(obj1, target) {
      let proto = Object.getPrototypeOf(obj1);
    do {
        if(proto == target.prototype) { // obj1.constructor.prototype == target.prototype
            return true;                // obj1.__proto__ == target.prototype
        } else {
            proto = Object.getPrototypeOf(proto);
        }
    } while(proto);
    return false;
  };
  
  // Custom class
  class CustomError extends Error { }
  
  // Native and custom class instances
  const nativeErr = new Error('erorr message');
  const customErr = new CustomError(2, 'code error message');

//   console.log(isInstanceOf(customErr, Number));
  instanceOf(nativeErr, Error);
  // Assertions
  console.assert(nativeErr instanceof Error === instanceOf(nativeErr, Error) );
  console.assert(customErr instanceof CustomError === instanceOf(customErr, CustomError) );
  
  console.assert(customErr instanceof Error === instanceOf(customErr, Error) );
  console.assert(customErr instanceof Object === instanceOf(customErr, Object) );
  console.assert(customErr instanceof Number === instanceOf(customErr, Number) );
  
  const arr = [];
  console.assert( arr instanceof Array === instanceOf(arr, Array));
  console.assert( arr instanceof Object === instanceOf(arr, Object) );
  
  // All assertions succsess
  console.log('OK!') */
  
  /* let arr = [1, 2, 10, 8, 9, 50];
  arr.sort((a, b) => a-b );

//   console.log(max, prevMax);
  console.log(arr[arr.length-1], arr[arr.length-2]); */

  /* for(var i = 0; i < 10; i++) {
      setTimeout((i) => console.log(i), 10, i);
  } */
