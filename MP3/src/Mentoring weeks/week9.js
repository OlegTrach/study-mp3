//  WEEK 9

// Scope and context
// Everything you wanted to know about JavaScript scope [Todd Motto]
// https://toddmotto.com/everything-you-wanted-to-know-about-javascript-scope/
// Understanding Scope in JavaScript [Hammad Ahmed]
// https://scotch.io/tutorials/understanding-scope-in-javascript

// Functional Scope
// Know global scope and functional scope
// Know variables visibility areas
// Understand nested scopes and able work with them

// Closures Advanced
// Context (lexical environment)
// Understand function creation context (lexical environment)
// Be able to explain difference between scope and context
/*     Scope pertains to the visibility of variables, and context 
    refers to the object to which a function belongs. */
// Inner/outer lexical environment
// Understand lexical environment traversing mechanism
// Understand connection between function and lexical environment
// Be able to discover cases where lexical environment required
// Be able to create and use closures

// ECMAScript Data Types & Expressions
// Object [keys/values]
/* Object.keys, values, entries
        For plain objects, the following methods are available:

        Object.keys(obj) – returns an array of keys.
        Object.values(obj) – returns an array of values.
        Object.entries(obj) – returns an array of [key, value] pairs. */

// Object calculated props
// [Symbol] data type

/* 'use strict';

let isAdmin = Symbol("isAdmin");

let user = {
  name: "Вася",
  [isAdmin]: true
};

alert(user[isAdmin]); // true */

/* Символы – новый примитивный тип, предназначенный для уникальных идентификаторов.
Все символы уникальны. Символы с одинаковым именем не равны друг другу.
Существует глобальный реестр символов, доступных через метод Symbol.for("name").
Для глобального символа можно получить имя вызовом Symbol.keyFor(sym).

Основная область использования символов – это системные свойства объектов, 
которые задают разные аспекты их поведения. Поддержка у них пока небольшая, 
но она растёт. Системные символы позволяют разработчикам стандарта добавлять 
новые «особые» свойства объектов, при этом не резервируя соответствующие строковые 
значения.

Системные символы доступны как свойства функции Symbol, например Symbol.iterator.

Мы можем создавать и свои символы, использовать их в объектах. Записывать их как 
свойства Symbol, разумеется, нельзя. Если нужен глобально доступный символ, то 
используется Symbol.for(имя). */

// Know [Symbol] data type specific
/* 
а) уникальны,
б) не участвуют в циклах,
в) заведомо не сломают старый код, который о них слыхом не слыхивал.

Продемонстрируем отсутствие конфликта для нового системного свойства 
Symbol.iterator:

'use strict';

let obj = {
  iterator: 1,
  [Symbol.iterator]() {}
}

alert(obj.iterator); // 1
alert(obj[Symbol.iterator]) // function, символ не конфликтует

// один символ в объекте
alert( Object.getOwnPropertySymbols(obj)[0].toString() ); // Symbol(Symbol.iterator)

// и одно обычное свойство
alert( Object.getOwnPropertyNames(obj) ); // iterator */

// Be able to explain difference between usual object key and symbol
// [Set/Map] data types
    /* Map is a collection of keyed data items, just like an Object. 
    But the main difference is that Map allows keys of any type.

    The main methods are:

        new Map() – creates the map.
        map.set(key, value) – stores the value by the key.
        map.get(key) – returns the value by the key, undefined if key doesn’t exist in map.
        map.has(key) – returns true if the key exists, false otherwise.
        map.delete(key) – removes the value by the key.
        map.clear() – clears the map
        map.size – returns the current element count. */
    
    // Iteration
        /* map.keys() – returns an iterable for keys,
        map.values() – returns an iterable for values,
        map.entries() – returns an iterable for entries [key, value], 
        it’s used by default in for..of. */

        /* A Set is a collection of values, where each value may occur only         once.

        Its main methods are:
        
            new Set(iterable) – creates the set, optionally from an array of values (any iterable will do).
            set.add(value) – adds a value, returns the set itself.
            set.delete(value) – removes the value, returns true if value existed at the moment of the call, otherwise false.
            set.has(value) – returns true if the value exists in the set, otherwise false.
            set.clear() – removes everything from the set.
            set.size – is the elements count.*/

// [WeakSet/WeakMap] data types
    /* Collections that allow garbage-collection:

    WeakMap – a variant of Map that allows only objects as keys and removes them once they become 
    inaccessible by other means. It does not support operations on the structure as a whole: no 
    size, no clear(), no iterations.

    WeakSet – is a variant of Set that only stores objects and removes them once they become 
    inaccessible by other means. Also does not support size/clear() and iterations.

    WeakMap and WeakSet are used as “secondary” data structures in addition to the “main” object 
    storage. Once the object is removed from the main storage, if it is only found in the 
    WeakMap/WeakSet, it will be cleaned up automatically. */
    
    /* WeakMap does not support iteration and methods keys(), values(), entries(), so there’s no way to get all keys or values from it.

    WeakMap has only the following methods:

        weakMap.get(key)
        weakMap.set(key, value)
        weakMap.delete(key, value)
        weakMap.has(key) */
    
const fruitColor = new Map()
    .set("red", ["apple", "strawberry"])
    .set("yellow", ["banana", "pineaple"])
    .set("purple", ["grape", "plum"]);

function test(color) {
    return fruitColor.get(color) || [];
}

