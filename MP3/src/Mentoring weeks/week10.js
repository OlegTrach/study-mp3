// WEEK 10

// JavaScript Errors
// [try..catch] statement
// Know how to handle errors
// Be able to explain [try..catch] performance issues
// Throw errors
// Custom errors

/* let data = '{ "age": 30 }'; 
try {
  let user = JSON.parse(data); // <-- no errors

  if (!user.name) {
    throw new SyntaxError("Bad data");
  }

  alert( user.name );

} catch (e) {
  alert( "Sorry something wrong with data" );
} */

// Timers
// [setTimeout]
// [setInterval]
/* var timerId = setInterval(func / code, delay[, arg1, arg2...]) */
// [clearTimeout]
/*     var timerId = setTimeout(...);
    clearTimeout(timerId); */

// [requestAnimationFrame]
    // https://medium.com/@stasonmars/%D0%BF%D1%83%D1%82%D0%B5%D1%88%D0%B5%D1%81%D1%82%D0%B2%D0%B8%D0%B5-%D0%BF%D0%BE-javascript-%D1%82%D0%B0%D0%B8%CC%86%D0%BC%D0%B5%D1%80%D0%B0%D0%BC-%D0%B2-%D1%81%D0%B5%D1%82%D0%B8-a8ef0e13e18
    // https://medium.com/@stasonmars/понимание-таймеров-в-javascript-callback-функции-settiфmeout-setinterval-и-requestanimationframe-f73c81cfdc9d
    // Understand [requestAnimationFrame] concept
// Be able to explain difference between [setTimeout] and [requestAnimationFrame]

// Event Loop
// Tasks, microtasks, queues and schedules
// https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
// Philip Roberts: What the heck is the event loop anyway? | JSConf EU
// https://youtu.be/8aGhZQkoFbQ
// How JavaScript works in browser and node?
// https://itnext.io/how-javascript-works-in-browser-and-node-ab7d0d09ac2f
// Jake Archibald: In The Loop - JSConf.Asia 2018
// https://youtu.be/cCOL7MC4Pl0
// Further Adventures of the Event Loop - Erin Zimmer - JSConf EU 2018
// https://youtu.be/u1kqx6AenYw

// Function.prototype.myBing = function () {
//   var fn = this,
//     arg = Array.prototype.slice.call(arguments),
//     obj = arg.shift();

//   return function () {
//     return fn.apply(
//       obj, arg.concat(Array.prototype.slice.call(arguments))
//     )
//   }
// }

// function a() { 
//   let c = 10; 
  
//   function b() { return c * 2;};
//   b(); 
// }; 

// console.log(a());

// function foo1() {
//   return {
//     bar: "hello"
//   };
// }

// function foo2() {
//   return (
//     {
//     bar: "hello"
//   });
// }

// foo2();

function arrModify(arr, currIndex, futureIndex) {
  const arr1 = [...arr];
  const element = arr1.splice(currIndex-1, 1);
  arr1.splice(futureIndex, 0, element[0]);

  return arr1;
}

function modifyArray(arr, currIndex, newIndex) {
  const res = [...arr];
  const el = res.splice(currIndex, 1);
  res.splice(newIndex, 0, el[0]);
  return res;
  }
  
  const array = ['first', 'second', 'third', 'fourth', 'fifth'];
  console.log(modifyArray(array, 1, 3));


/*   const obj = {
    foo: 'a',
    ['foo']: 'bla',
    [false]: 'b',
  };
  console.log(obj.foo);
  console.log(obj); */

  // remove doubles in arrays

  const doubleArray = [1, 2, 3, 4, 5, 1, 2, 3, 6, 1, 3];

  function removeDoubles(arr) {
      return [...new Set(arr)]
  }
  
  console.log(removeDoubles(doubleArray));
  
  function removeDoubles2(arr) {
      return arr.filter((el, i) => arr.indexOf(el) === i);
  }
  
  console.log(removeDoubles2(doubleArray));

  // promises

let promise =  new Promise((resolve, rejected) => {
  setTimeout(() => {
    resolve('Hello promise');
  }, 2000);

  setTimeout(() => {
    rejected(new Error('Error called!'))
  }, 1000)
});

promise.then(
  result => {
    console.log(result);
    return 'Hello from first then!'
  })
  .catch(error => console.log(error))
  // .then(resultText => console.log(resultText))
 

// xhr

let xhr = new XMLHttpRequest();

xhr.open('GET', 'https://jsonplaceholder.typicode.com/users/6', true);
xhr.send();

xhr.onreadystatechange = () => {
  if (xhr.status === 200) {
    if (xhr.readyState === 4) {
      console.log(JSON.parse(xhr.responseText));
    }
  } else {
    alert(`${xhr.status}: ${xhr.stat}`)
  }
}

// use info function

function userInfo(url) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', url);

    xhr.onreadystatechange = () => {
      if (xhr.status === 200) {
        if (xhr.readyState === 4)
          resolve(xhr.response);
      } else {
        let error = new Error(this.statusText);
        error.code = this.status;
        reject(error);
      }
    };

    xhr.onerror = function () {
      reject(new Error('Network Error'));
    };

    xhr.send();
  });
}

userInfo('https://jsonplaceholder.typicode.com/users/6')
.then(
  response => console.log(`Fullfiled:`, response),
  error => console.log(`rejected:`, error)
);

//  user info thrue fetch
let url = 'https://jsonplaceholder.typicode.com/users/6';
let options = { method: 'GET' }
let req = fetch(url, options); // produces get request by default


req.then(response => response.json()) // returns promise
  .then(data => console.log(data))
  .catch(err => { console.log(err); })

// Promise all

function userInfo(url) {
  return fetch(url);
}

Promise.all([
  userInfo('https://jsonplaceholder.typicode.com/users/4'),
  userInfo('https://jsonplaceholder.typicode.com/posts/4'),
  userInfo('https://jsonplaceholder.typicode.com/photos/4'),
])
.then(responses => Promise.all(responses.map(res => res.json())))
.then(x => console.log(x))
.catch(error => console.error());

// Promise race

function userInfo(url) {
  return fetch(url);
}

Promise.race([
  userInfo('https://jsonplaceholder.typicode.com/users/4'),
  userInfo('https://jsonplaceholder.typicode.com/posts/4'),
  userInfo('https://jsonplaceholder.typicode.com/photos/4'),
])
.then(response => response.json())
.then(x => console.log(x))
.catch(error => console.error());

//  axios


const url = 'https://api.spotify.com/v1/artists/0OdUWJ0sBjDrqHygGUXeCF'
axios({
  method:'get',
  url: url,
  responseType:'stream'
})

axios.get(url).then(response => console.log(response));


// Polling 

function refresh() {
  // make Ajax call here, inside the callback call:
  setTimeout(refresh, 5000);
  // ...
}

// initial call, or just call refresh directly
setTimeout(refresh, 5000);

// Regular polling

(function poll(){
  setTimeout(function(){
     $.ajax({ url: "server", success: function(data){
       //Update your dashboard gauge
       salesGauge.setValue(data.value);

       //Setup the next poll recursively
       poll();
     }, dataType: "json"});
 }, 30000);
})();

// Long polling

// client side code
function subscribe(url) {
  var xhr = new XMLHttpRequest();

  xhr.onreadystatechange = function() {
    if (this.readyState != 4) return;

    if (this.status == 200) {
      onMessage(this.responseText);
    } else {
      onError(this);
    }

    subscribe(url);
  }
  xhr.open("GET", url, true);
  xhr.send();
}