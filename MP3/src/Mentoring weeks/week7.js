// WEEK 7
// *****Advanced Expressions*****

// Hoisting
// Understand hoisting concept
// Able to use hoisting in development
// Auto data type conversion
// Know rules of auto data types conversion
// Be able to discover cases of implicit data types conversion into boolean, string, number
// Strict comparison
// Be able to discover difference between strict and non-strict comparison
// Be able to provide use cases of both types of comparison
// alert( 0 == false ); alert( 0 == false ); // true

/* Comparison operators return a logical value.
Strings are compared letter-by-letter in the “dictionary” order.
When values of different types are compared, they get converted to
numbers (with the exclusion of a strict equality check).
Values null and undefined equal == each other and do not equal any
other value.
Be careful when using comparisons like > or < with variables that 
can occasionally be null/undefined. Making a separate check for 
null/undefined is a good idea. */

// *****Functional Patterns*****

// Immediate function
// Know immediate function pattern
// Be able to explain the purposes of immediate function usage
// Callback (Function as argument)
// Know callback pattern
// Understand callback limitations (callback hell)
// Binding
// Know how to bind [this] scope to function
// Be able to provide cases when it's required