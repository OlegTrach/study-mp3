// WEEK 6
// *****Functional Inheritance*****

// ____________________________________________________
/* !!!When you use the new keyword in JavaScript…!!!
1. A Brand New Object Gets Created
2. The Created Object Gets [[Prototype]] linked
3. The Created Object is set to this for that function call
4. this (the newly constructed object) is automatically 
    returned from the new-invoked function, unless it explicitly 
    returns a different object */
//_____________________________________________________

// Factory function

/* function createCircle(radius) {
    return {
        radius,
        draw: function() {
            console.log("draw");
        }
    }
}
const circle = createCircle(1);
circle.draw(); */

// Constructor function

/* function Circle(radius) {
    function draw() {
        console.log("draw")
    }

    this.radius = radius;
    this.draw = draw;
}
const another = new Circle(1); */

// Resetting the constructor
function Shape(color) {
    this.color = color
}

Shape.prototype.duplicate = function() {
    console.log("duplicate");
}

function Circle(radius, color) {
    // call super constructor
    Shape.call(this, color);
    this.radius = radius;
}

// when you reset the proptotype of the circle 
// reset the constructor as well !!!
Circle.prototype = Object.create(Shape.prototype);
Circle.prototype.constructor = Circle;

Circle.prototype.draw = function() {
    console.log("draw");
}

function Square(size ) {
    this.size = size;
}

Square.prototype = Object.create(Shape.prototype);
Square.prototype.constructor = Square;

const s = new Shape();
const c = new Circle(1, "red");

// _______________________________________________________

// quick guide Functional pattern inheritance

// 1. The constructor of the parent Machine is declared. 
// It can have private (private), public (public) and 
// protected (protected) properties:

/* function Machine(params) {
    // local variables and functions are available only 
    // within(inside) the Machine
    var privateProperty;
  
    // public available outside
    this.publicProperty = ...;
  
    // protected are available inside the 
    // Machine and for descendants
    // we agree not to touch them outside
    this._protectedProperty = ...
  }
  
  var machine = new Machine(...)
  machine.public(); */

//   2. To inherit, the constructor of the child calls the 
//     parent in its context through apply. 
//     Then you can add your variables and methods:

/* function CoffeeMachine(params) {
    // universal call with passing any arguments
    Machine.apply(this, arguments);
  
    this.coffeePublicProperty = ...
  }
  
  var coffeeMachine = new CoffeeMachine(...);
  coffeeMachine.publicProperty();
  coffeeMachine.coffeePublicProperty(); */

//   3. In CoffeeMachine, properties obtained from a parent 
//     can be overwritten with your own. But usually it is 
//     required not to replace, but to expand the method of 
//     the parent. To do this, it is pre-copied into the 
//     variable:

/* function CoffeeMachine(params) {
    Machine.apply(this, arguments);
  
    var parentProtected = this._protectedProperty;
    this._protectedProperty = function(args) {
      parentProtected.apply(this, args); // (*)
      // ...
    }; 
  }*/

//   The string (*) can be simplified to parentProtected 
//   (args), if the parent method does not use this, but, 
//   for example, is bound to var self = this:

/* function Machine(params) {
    var self = this;
  
    this._protected = function() {
      self.property = "value";
    };
  } */

//   It must be said that the mode of inheritance described 
//   in this chapter is used infrequently.

// ________________________________________________________________

// 


// Rent a constructor
// Know functional inheritance pattern
// Be able to explain difference between functional and prototypal inheritance
// Be able to discover benefits and drawback of both prototypal and functional inheritance

// _________________________________________________________________
/* Is see the following advantages of the use of the functional inheritance pattern:

functional inheritance
simplicity: a few steps are required to achieve inheritance ;
“this”-free: you don’t have to care with the context variable “this”, 
meaning that you can freely pass any method of a functional object as a 
callback parameter and everything will work as expected (see this script, for example) ;
encapsulation: objects can have private (and even protected) members. 

prototypal inheritance pattern:
performance: prototypal object creation involves less steps than functional objects creation, 
thus prototypal object creation is faster than functional object creation (by several orders 
of magnitude, as you’ll see below) 
dynamsim: one can add methods to a prototype at any time and these methods will automatically 
be available on all objects inheriting this prototype ;
reflection: though this is not very important, you can test if an object is of some type by 
using the instanceof operator, which is not feasible with functional inheritance.


Функциональный стиль записывает в каждый объект и свойства и методы, а прототипный – 
только свойства. Поэтому прототипный стиль – быстрее и экономнее по памяти.

При создании методов через прототип, мы теряем возможность использовать локальные 
переменные как приватные свойства, у них больше нет общей области видимости с конструктором.
*/

// __________________________________________________________________

// Mix-ins
// Know mix-in pattern
// Know mix-in specific and limitations
// Able to explain benefits and drawbacks comparing with inheritance


// *****ECMAScript Intermediate*****

// Function default parameters
// Discover default parameters concept and limitations
// Spread operator for Function
// Know how to use spread operator for Function arguments
// Be able to compare [arguments] and spread operator
// Spread operator for Array
// Understand and able to use spread operator for Array concatenation
// Destruction
// Be able to discover destruction concept
// Understand variables and Function arguments destruction
// String templates
// Know String template syntax and rules
// [for..of] loop
// Know how [for..of] loop works
// Be able to compare [for..of] loop with other types of loops
