// Functions

// Function declaration/expression
// Know both Function expression and Function declaration
// Be able to explain declaration/expression differences

// ES 6 Classes

// Class declaration
// Know [class] declaration syntax
// Know how [class] declaration works under the hood
// Understand difference between [class] and function constructor
// Understand difference between method and [class] method
// Be able to develop in OOP style using [class] declaration
// [constructor] keyword
// Inheritance
// Know [extends] syntax and how it works
// Getter/setter
// Be able to create getter / setter [class] methods

class Car {
    constructor(maker, model) {
      this.maker = maker;
      this.model = model;
    }
    drive() {
        console.log("Zoom!");
    }
  }
  
  class Tesla extends Car {
    constructor(model, chargetime) {
        super('Tesla', model);
        this.chargetime = chargetime;
    }
    charge() {
        console.log("Charging...");
    }
}

const tesla = new Tesla('3', 20);
tesla.drive();
tesla.charge();
console.log("tesla.__proto__", tesla.__proto__);  //Tesla {}

// set prototype 

// by __proto__
let animal = {
    eats: true
};
let rabbit = {
    jumps: true
};

rabbit.__proto__ = animal; // (*)

// we can find both properties in rabbit now:
console.log(rabbit.eats); // true (**)
console.log(rabbit.jumps); // true

// by prototype

let animal1 = {
    eats: true
};

function Rabbit(name) {
    this.name = name;
}

Rabbit.prototype = animal1;

let rabbit1 = new Rabbit("White Rabbit"); //  rabbit1.__proto__ == animal1

console.log(rabbit1.eats); // true

function Truck(brand) {
    this.brand = brand;
    function turn1() {
        console.log("turn static");
    };
    this.turn = () => {console.log("this is turn")}
};

let truck = new Truck();
truck.turn();

// By default all functions have F.prototype = { constructor: F },
// so we can get the constructor of an object by accessing its "constructor" property.

function Rabbit() {}
// by default:
// Rabbit.prototype = { constructor: Rabbit }

console.log( Rabbit.prototype.constructor == Rabbit ); // true

// Getters and setters with classes

class Player {
    constructor(id) {
      this.id = id;
      this.cashPlayer = 350;
    }
  
    get cash() {
      return this.cashPlayer;
    }
  
    set cash(value) { // line 19
      this.cashPlayer = value; // line 20
    }
}

let player = new Player();
console.log(player.cash);
player.cash = 500;
console.log(player.cash);

// Function advance

// arguments
// Understand [arguments] and dynamic amount of parameters
// Be able to use [arguments], retrieve additional parameters
// [this] scope
// Understand difference between function and method
// Understand how [this] works, realize [this] possible issues
// Manage [this] scope
// Be able to replace [this] scope
// Be able to use [call] and [apply] Function build-in methodss

function reverse1(n) {
    // Array#reverse method takes no argument.
    // You can use `Math.abs()` instead of changing the sign if negative.
    // Conversion of string to number can be done with unary plus operator.
    var reverseN = +String(Math.abs(n)).split('').reverse().join('');
    // Use a number constant instead of calculating the power
    if (reverseN > 0x7FFFFFFF) { // chesk if n in range range: [−2^31,  2^31 − 1] 32-bit signed integer
        return 0;
    }
    // As we did not change the sign, you can do without the boolean isNegative.
    // Don't multiply with -1, just use the unary minus operator.
    // The ternary operator might interest you as well (you could even use it
    //    to combine the above return into one return statement)
    return n < 0 ? -reverseN : reverseN;
}

console.log(reverse1(-254));


var isPalindrome = function(x) {
    let str = x.toString();
      for(let i=0, j=str.length-1; i<=j, j>=i; i++, j--){
          if(str[i] !== str[j]){
              return false
          }
      }
    return true
};

console.log("oko", isPalindrome("oko oko oko"));
console.log("125", isPalindrome(125));
console.log("-125", isPalindrome(-125));