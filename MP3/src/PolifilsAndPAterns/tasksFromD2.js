// Hoisting
// 1) (not strict mode)
/* console.log(a); //a is not defined */
// 2)
    /* console.log(a) //undefined
    var a; */
// 3) 
    // console.log(a) //function a
    // function a() {}
    // var a;
    // console.log(a) // function a
// 4) 
    /* console.log(a) //function
    function a() {}
    var a = 3;
    console.log(a); //3 */

    // Lexical Scope
// 5) 
    /* var a = 3;

    function foo() {
        var a = 5;
        bar();
    }

    function bar() {
        console.log(a);
    }

    foo(); //3 */

// 6) 
    /* var a = 3;

    function foo() {
        a = 5;
        bar();
    }

    function bar() {
        console.log(a);
    }

    foo(); //5 */

// Context
// 7) 
    /* var a = 7;

    function g() {
        var a = 3;

        function h() {
            console.log(this.a);
        }
        return h();
    }
    g(); // 7 */

// 8)
    /* var a = {
        b: 5,
        c: this.b,
        d: function () {
            console.log(this.b)
        },
        e: () => console.log(this.b)
    };
    var y = a.d;
    y(); //undefined */

    // Closure
// 9) 
    /* var arr = [];
    for (var i = 0; i < 5; i++) {
        arr[i] = function () {
            console.log(i);
        }
    }
    arr[3] = ? */
// console.log(arr[3]()) => 5

// Fix it.
// 10) 
// currying function what is it and what for
// Tasks
// 11) palyndrome: optimal way;
// 12) deepFlatten: [1, 2, [3, 4 [5, 6, 7]]] => [1, 2, 3, 4, 5, 6, 7];

var deepFlatten = function (array) {
    var result = [];

    array.forEach(function (elem) {
        if (Array.isArray(elem)) {
            result = result.concat(deepFlatten(elem)); // Fix here
        } else {
            result.push(elem);
        }
    });
    return result;
};

console.log(deepFlatten([1, 2, [3, 4, [5, 6, 7]]]));

var deepFlattenS = function (array) {
    return array.reduce(function (r, e) {
        return Array.isArray(e) ? r.push(...deepFlattenS(e)) : r.push(e), r
    }, [])
};

console.log(deepFlattenS([1, 2, [3, 4, [5, 6, 7]]]));

function steamrollArray(arr) {
    let flat = [].concat(...arr);
    return flat.some(Array.isArray) ? steamrollArray(flat) : flat;
}

steamrollArray([1, [2], [3, [[4]]]]); //[1, 2, 3, 4]

// Algorythm Fibonachi

const fibonachi = n => {
  let prev = 0;
  let next = 1;
  for(let i = 0; i < n; i++){
    let temp = next;
    next = prev + next;
    prev = temp;
  }
  return prev;}

console.log(fibonachi(50));
