// POLYFILL BIND
Function.prototype.bind = function() {
    var fn = this,
        args = Array.prototype.slice.call(arguments),
        object = args.shift();
    return function() {
        return fn.apply(object, args.concat(Array.prototype.slice.call(arguments)));
    };
};

// POLYFILL forEach

Array.prototype.myForEach = function(callback) {
    for(var i = 0; i < this.length; i++)
        callback(this[i], i, this);
};

var arr = ["one", "two", "three", "four", "five"];
arr.myForEach(function(word) {
    console.log("word", word);
})

// POLYFILL MAP

Array.prototype.myMap = function(callback) {
    arr = [];
    for(var i = 0; i < this.length; i++)
        arr.push(callback(this[i], i, this));
    return arr;
};

var arrs = ["Mum", "Dad", "Grandpa", "Grandma"];
var numbers1 = [1, 2, 3, 4, 5];

var relatives = arrs.myMap(function(n){
    return n;
});

var doubles = numbers1.myMap(function(num){
    return num * num;
});

console.log(relatives);
console.log(doubles);

// POLYFILL FILTER

Array.prototype.myFilter = function(callback){
    arr = [];
    for(var i = 0; i < this.length; i++) {
        if(callback(this[i], i, this))
            arr.push(this[i]);
    }
    return arr;
};

var numbers2 = [1, 20, 30, 80, 2, 9, 3];
var newNum = numbers2.myFilter(function(n){
    return n >= 10;
});
console.log(newNum);

// POLYFILL REDUCE

Array.prototype.myReduce = function(callback, initialVal) {
    var accumulator = (initialVal === undefined) ? this[0] : initialVal;
    
    for (var i = initialVal ? 0 : 1; i < this.length; i++) {
        accumulator = callback(accumulator, this[i], i, this);
    }

    return accumulator;
};

var numbers3 = [20, 20, 2, 3];
var total = numbers3.myReduce(function(a, b){
    return a + b;
}, 10);

console.log(total);

// POLYFILL EVERY 

Array.prototype.myEvery = function(callback) {
    for(var i = 0; i < this.length; i++){
        if(!callback(this[i], i, this))
            return false;
    }
    return true;
};

var passed = [12, 5, 8, 130, 44].myEvery(function(element) {
    return (element >= 10);
});
console.log(passed);


// POLYFILL SOME

Array.prototype.mySome = function(callback) {
    for(var i = 0; i < this.length; i++){
        if(callback(this[i], i, this))
            return true;
    }
    return false;
};

var passed1 = [12, 5, 8, 130, 44].mySome(function(element){
    return (element >= 200);
});

console.log(passed1);

// POLYFILL INCLUDES

Array.prototype.myIncludes = function(element, startIndex) {
    if (!this.length || (startIndex && this.length < startIndex)) return;

    for(var i = startIndex || 0; i < this.length; i++) {
        if(element === this[i]) {
            return true;
        }
    }
    return false;
}

var arr = [1, 2, 3, 4, 5];

var res = arr.myIncludes(5, 0);

console.log(res);

// POLYFILL FLAT
Array.prototype.myFlat = function (depth = Infinity) {
    return flatten(this, depth)
}

function flatten(arr, depth) {
    if (depth === 0) {
        return arr
    }
    return arr.reduce((accumulator, item) => {
        if (Array.isArray(item)) {
            accumulator.push(...flatten(item, depth - 1))
        } else {
            accumulator.push(item)
        }
        return accumulator
    }, [])
}

console.log([1, 2, [3, 4, [5, 6, 7]]].myFlat());

// POLYFILL FLATMAP

Array.prototype.flatMap = function (fn, context) {
    return flattenMap(this, 1, fn, context)
}

function flattenMap(list, depth, mapperFunction, mapperContext) {
    if (depth === 0) {
        return list
    }
    return list.reduce((accumulator, item, i) => {
        if (mapperFunction) {
            item = mapperFunction.call(mapperContext || list, item, i, list)
        }
        if (Array.isArray(item)) {
            accumulator.push(...flattenMap(item, depth - 1))
        } else {
            accumulator.push(item)
        }
        return accumulator
    }, [])
}

const array1 = [{ x: 1, y: 2 }, { x: 3, y: 4 }, { x: 5, y: 6 }]

console.log(array1.flatMap(c => [c.x, c.y]));

// PROMISES
const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        if (true) {
            resolve("Hello from promise")
        } else {
            reject()
        }
    }, 1)
})

promise
    .then(data => console.log("success: ", data)) // first variant
    .catch(error => console.log("error: ", error));

    // .then(
    //     data => console.log("success: ", data), // second variant, change time in timeout
    //     error => console.log("error: ", error)
    // ) 

// ASYNC/AWAIT
const fetchProducts = () => Promise.resolve({data: [1, 2, 3]});
const fetchAdditional = () => Promise.resolve({data: [2, 3, 4]})

const getProducts = async () => {
    const products = await fetchProducts();

    if(!products.data.length) {
        return products.data
    }

    return await fetchAdditional(products.data)
}

getProducts().then(result => {
    console.log("result - ", result);
});

// ASYNC/AWAIT second
function getUser(id) {
    return Promise.resolve({ id : 1 });
}

async function main() {
    let user = await getUser(1); // await ставиться перед будь якою функцією що повертає promise
    console.log(user);
}

main();

// Async/await before and now 

function one() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log("Done!");
            resolve("2 secons have passed!");
        }, 2000)
    })
}

async function asyncAwait() {
    console.log("Calling function asyncAwait");
    const oneResponse = await one();
    console.log("one response", oneResponse); // wait untill promise resolve
}

function promises() {
    console.log("Calling function promises");
    one().then((promiseData) => {
        console.log("one response", promiseData);
    });
}

asyncAwait();
promises();
console.log("Last line of code");

// Works with Arrays (helper methods)

// MEMOIZATION

// a simple pure function to get a value adding 10
const add = (n) => (n + 10);
console.log('Simple call', add(3));
// a simple memoize function that takes in a function
// and returns a memoized function
const memoize = (fn) => {
  let cache = {};
  return (...args) => {
    let n = args[0];  // just taking one argument here
    if (n in cache) {
      console.log('Fetching from cache');
      return cache[n];
    }
    else {
      console.log('Calculating result');
      let result = fn(n);
      cache[n] = result;
      return result;
    }
  }
}
// creating a memoized function for the 'add' pure function
const memoizedAdd = memoize(add);
console.log(memoizedAdd(3));  // calculated
console.log(memoizedAdd(3));  // cached
console.log(memoizedAdd(4));  // calculated
console.log(memoizedAdd(4));  // cached

// *****Flatten array*****

function steamrollArray(arr) {
    let flat = [].concat(...arr);
    return flat.some(Array.isArray) ? steamrollArray(flat) : flat;
  }
  
  steamrollArray([1, [2], [3, [[4]]]]); //[1, 2, 3, 4]

//   *****Object methods*****

// Object.keys, values, entries
// For plain objects, the following methods are available:

// Object.keys(obj) – returns an array of keys.
// Object.values(obj) – returns an array of values.
// Object.entries(obj) – returns an array of [key, value] pairs.

   /*  let user = {
        name: "John",
        age: 30
    };

    Object.keys(user) = [name, age]
    Object.values(user) = ["John", 30]
    Object.entries(user) = [ ["name","John"], ["age",30] ] */

// Here’s an example of using Object.values to loop over property values:

/* let user = {
    name: "John",
    age: 30
};

// loop over values
for (let value of Object.values(user)) {
    alert(value); // John, then 30
} */

function bubbleSort(arr) {
    let swap;

    do {
        swap = false;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] > arr[i + 1]) {
                arr[i] = arr[i] + arr[i + 1];
                arr[i + 1] = arr[i] - arr[i + 1];
                arr[i] = arr[i] - arr[i + 1];
                swap = true;
            }
        }
    } while (swap);
    return arr;
}

let arrForSort = [1, 4, 9, 5, 3, 2, 0, 8, 7, 10];
console.log("bublleSort", bubbleSort(arrForSort));

// link = https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects

// Object.getOwnPropertyNames(object1)); // expected output: Array ["a", "b", "c"]

// Binary search O(log(n))

let steps = 0;

const binarySearch = (array, value) => {
    const length = array.length;
    const middle = Math.floor(length / 2);
    const middleElement = array[middle];

    steps++;

    if (length === 1) {
        if (array[0] === value) {
            return true;
        } else {
            return false;
        }
    }

    if (middleElement > value) {
        const leftSide = array.slice(0, middle + 1);
        // console.log('leftSide: ', leftSide)
        return binarySearch(leftSide, value);
    } else if (middleElement < value) {
        const rightSide = array.slice(middle, length);
        // console.log('rightSide: ', rightSide)
        return binarySearch(rightSide, value);
    } else {
        console.log('steps: ' + steps);
        return true;
    }
};

console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 7))

// Algorythm Fibonachi

const fibonachi = n => {
  let prev = 0;
  let next = 1;
  for(let i = 0; i < n; i++){
    let temp = next;
    next = prev + next;
    prev = temp;
  }
  return prev;}

console.log(fibonachi(50));