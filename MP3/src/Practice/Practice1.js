/* class Animal {
    constructor(type, color) {
        this.type = type;
        this.color = color;
    }

    run() {
        console.log("Hello, I am " + this.type + ". I have a " 
        + this.color + " color and I can run!");
    }
}

class Rabbit extends Animal {
    constructor(name) {
        super("Rabbit", "grey");
        this.name = name;
    }

    jump() {
        console.log("Hello my name is " + this.name + ".");
    }
}

const rabbit = new Rabbit("Bunny");
rabbit.run();
rabbit.jump();

console.log("result Rabbit: ", rabbit);

// *****************************************

let Animal1 = {
    constructor: function(type, color) {
        this.type = type;
        this.color = color;
    },

    run: function() {
        console.log("Hello, I am " + this.type + ". I have a " 
        + this.color + " color and I can run!");
    }
}

function Rabbit1(name) {
    Animal1.constructor("Rabbit1", "grey");
    this.name = name;

    this.jump = () => {
        console.log("Hello my name is " + this.name + ".");
    }
}

const rabbit1 = new Rabbit1("Bunny1");
Object.setPrototypeOf(rabbit1, Animal1);
rabbit1.run();
rabbit1.jump();

console.log("result Rabbit1: ", rabbit1, Animal1);

// function isPalindrome(str) {
//     for (let i = 0, j = str.length-1; i<=j, j>=i; i++, j--) {
//         if(str[i] !== str[j]) {
//             return false
//         }
//     }
//     return true;
// }

function isPalindrome(str) {
    const strInv = str.split("").reverse().join("");
    return strInv === str;
}

console.log(isPalindrome("bala bala bla"));
console.log(isPalindrome("abla 1 alba"));
console.log(isPalindrome("abla 311 alba"));


function Machine(params) {
    var privateProperty = "private property";
    this.publicProperty = "public property";
    this._protectedProperty = "protected property";
}

var machine = new Machine();
console.log(machine.publicProperty);
console.log(machine);


function greet() {
    if(greet.locale === "ua") {
        console.log("Привіт!");
    } else if (greet.locale === "es") {
        console.log("Hola!");
    } else {
        console.log("Hello!");
        console.dir(arguments.callee.name);
        console.dir(arguments.callee.caller.name);
        
    }
}

greet();
greet.locale = "ua";
greet(); */

// 'use strict';

/* function showThis(a, b) {
    console.log(this);

    function sum() {
        console.log(this);
        return a + b;
    }

    console.log(sum());
}

showThis(4, 5);
showThis(5, 5); */

/* let user = {
    name: 'John'
};

function sayName() {
    console.log(this);
    console.log(this.name);
}

sayName.call(user); */

/* Function.prototype.myBind = function () {
    const fn = this;
    const args = Array.prototype.slice.call(arguments);
    const obj = args.shift();

    return function() {
        return fn.apply(obj, args.concat(Array.prototype.slice.call(arguments)));
    }
}

const myObject = {
    x: "My value from myObject"
};

const foo = function() {
    console.log(this.x);
}.myBind(myObject);

foo(); */

// ********

/* function drawStairs(h) {
    let line = "";

    for (var i = 1; i <= h; i++) {
        console.log(line.padStart(h - i, " ").padEnd(h, "#"));
    }
}

drawStairs(5); */

// *********

/* let obj = {
    a: 15,
    b: function() {console.log(this, this.a)},
    c: () => {console.log(this, this.a)},
    d: function() {
        const e = () => {
            console.log(this, this.a)
        };
        e();
    },
    f: () => {
        function g() {
            console.log(this, this.a)
        }; 
        g();
    }
}

const tempB = obj.b;
const tempC = obj.c;

obj.b();
obj.c();
obj.d();
obj.f();

tempB();
tempC(); */

/* function a() {
    let c = 15;

    function b() {
        return c*2;
    }
    b();
}

a(); */

// Callback hell

const func1 = function(param, func2) {
    func2(function(param, func3) {
        func3(function(param, func4) {
            func4(function(param, func5) {})
        })
    })
}

// Promises

const promise = new Promise((resolve, reject) => {
    if (true) {
        resolve('Stuff Worked')
    } else {
        reject('error, it broke')
    }
})

const promise2 = new Promise((resolve, reject) => {
    setTimeout(resolve, 200, 'HIIII')
})

const promise3 = new Promise((resolve, reject) => {
    setTimeout(resolve, 1000, 'POOKIE')
})

const promise4 = new Promise((resolve, reject) => {
    setTimeout(resolve, 3000, 'Is it me you are looking for?')
})

Promise.all([promise, promise2, promise3, promise4]).then(values => {
    console.log(values)
})

// next Promises
//  run on browser console
const urls = [
    'https://jsonplaceholder.typicode.com/users',
    'https://jsonplaceholder.typicode.com/posts',
    'https://jsonplaceholder.typicode.com/albums',
]

Promise.all(
    urls.map(url => {
        return fetch(url).then(resp => resp.json())
    }),
).then(results => {
    console.log(results[0])
    console.log(results[1])
    console.log(results[2])
})

// Canceling Promices

// a basic stitched-together CancelToken
// creation mechanism
function createToken() {
    const token = {}
    let cancel

    token.promise = new Promise(resolve => {
        cancel = reason => {
            token.reason = reason
            resolve(reason)
        }
    })

    return {token, cancel}
}
// create a token and a function to use later.
const {token, cancel} = createToken()
// your functions that return promises would also take
// a cancel token
function delay(ms, token) {
    return new Promise(resolve => {
        const id = setTimeout(resolve, ms)
        token.promise.then(reason => clearTimeout(id))
    })
}

// Module

let number = 1

;(function() {
    let number = 2

    console.log(number)
    return console.log(number + 3)
})()

console.log(number)

// Module 2

const user = (function() {
    const privat = function() {
        console.log('I am privat')
    }

    return {
        sayHello: function() {
            console.log('Hello!')
        },
    }
})()

console.log(user)
console.log(user.sayHello())

Array.prototype.myReduce = function(callback, initialValue) {
    let accumulator = initialValue === undefined ? undefined : initialValue
    for (let i = 0; i < this.length; i++) {
        if (accumulator !== undefined) {
            accumulator = callback.call(
                undefined,
                accumulator,
                this[i],
                i,
                this,
            )
        } else {
            accumulator = this[i]
        }
    }
    return accumulator
}

var numbers3 = [20, 20, 2, 3]
// var total = numbers3.myReduce(function(a, b){
//     return a + b;
// }, 10);

// console.log(total);

console.log(
    Array.prototype.myReduce.call(
        numbers3,
        function(a, b) {
            return a + b
        },
        5,
    ),
)

class MyComponent extends React.Component {
    render() {
        return (
            <div>
                <h1>Hello</h1>
                <Header />
            </div>
        )
    }
}
class Header extends React.Component {
    render() {
        return
        ;<h1>Header Component</h1>
    }
}
ReactDOM.render(<MyComponent />, document.getElementById('content'))
// this
{
    const a = 'global'

    const obj = {
        a: 'inner a',
        b() {
            console.log(this.a)
        },
        c: () => {
            console.log(this)
        },
    }

    console.log(obj.b())
    console.log(obj.c())
}

// without ES5 Classes

function Person(name, age) {
    this.name = name
    this.age = age
}

Person.prototype.incrementAge = function() {
    return (this.age += 1)
}
function Personal(name, age, ocupation, hobby) {
    Person.call(this, name, age)
    this.ocupation = ocupation
    this.hobby = hobby
}

Personal.prototype = Object.create(Person.prototype) //перетерли конструктор
Personal.prototype.constructor = Personal
Personal.prototype.incrementAge = function() {
    Person.prototype.incrementAge.call(this)
    this.age += 20
    console.log(this.age)
}

const OBJinst = new Personal('Ivan', '45', 'carpenter', 'chess')
// console.log(OBJinst, OBJinst.constructor);

// with es5 calsses

class Person {
    constructor(name, adge) {
        this.name = name
        this.age = age
    }

    incrementAge() {
        this.age += 1
    }
}

class Personal extends Person {
    constructor(name, age, ocupation, hobby) {
        super(name, ege)
        this.ocupation = ocupation
        this.hobby = hobby
    }

    incrementAge() {
        super.incrementAge()
        this.age += 20
        console.log(this.age)
    }
}

// ******* Flatten
function fA(arr) {
    return arr.reduce((acum, el) => {
        return Array.isArray(el) ? acum.push(...fA(el)) : acum.push(el), acum
    }, [])
}

function fA(arr) {
    let res = []

    arr.forEach(el => {
        if (Array.isArray(el)) {
            res = res.concat(fA(el))
        } else {
            res.push(el)
        }
    })

    return res
}

console.log(fA([1, 2, 3, 4, [5, 6, [7, 8, 9, [10, 11, 12, [13, 14, 15]]]]]))

let sym1 = Symbol()
let sym2 = Symbol('foo')
let sym3 = Symbol('foo')

console.log(sym2 == sym3) // false
console.log(sym2 === sym3) //false

// Build chain of promices

;('use strict')

let urls = [
    'http://learn.javascript.ru/article/promise/user.json',
    'http://learn.javascript.ru/article/promise/guest.json',
]

// chain start
let chain = Promise.resolve()

let results = []

// add chain in loop
urls.forEach(function(url) {
    chain = chain
        .then(() => httpGet(url))
        .then(result => {
            results.push(result)
        })
})

// return results
chain.then(() => {
    console.log(results)
})

Promise.all(urls.map(httpGet)).then(alert)

// Delay promice result

function delayPromice(ms) {
    return Promise((resolve, reject) => {
        setTimeout(resolve, ms)
    })
}

// Reduce with promises

function methodThatReturnsAPromise(nextID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`Resolve!`)

            resolve()
        }, 1000)
    })
}

;[1, 2, 3].reduce((accumulatorPromise, nextID) => {
    console.log(`Loop!`)

    return accumulatorPromise.then(() => {
        return methodThatReturnsAPromise(nextID)
    })
}, Promise.resolve())

// Generator functions

function* generateSequence() {
    yield 10
    yield 50
    yield 100
}

let generator = generateSequence()

console.log('generator: ', generator)

let one = generator.next()
console.log('Results: one', one)
generator.next()

let three = generator.next()
let four = generator.next()

console.log('Results in the end: one', one)
console.log('Results: one, three, four', one, three, four)

//  XMLHttpRequest

var xhr = new XMLHttpRequest()

xhr.open('GET', '/my/url', true)

xhr.send()

xhr.onreadystatechange = function() {
    if (this.readyState != 4) return

    // after request we get:
    // status, statusText
    // responseText, responseXML (при content-type: text/xml)

    if (this.status != 200) {
        // error handling
        alert('error: ' + (this.status ? this.statusText : 'request failed'))
        return
    }

    //Get result from  this.responseText or this.responseXML
}

// sync await

function one() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Done!')

            resolve('2 secons have passed!')
        }, 2000)
    })
}

async function asyncAwait() {
    console.log('Calling function asyncAwait')
    const oneResponse = await one()
    console.log('test waiting')
    console.log('one response', oneResponse)
}

function promises() {
    console.log('Calling function promises')
    one().then(promiseData => {
        console.log('one response', promiseData)
    })
}

asyncAwait()
promises()
console.log('Last line of code')

var a = {},
    b = {key: 'b'},
    c = {key: 'c'}
a[b] = 123
a[c] = 456
console.log(a[c])

// find duplicate in array

function findDuplicate(nums) {
    let map = new Map()
    let res = []

    for (let i = 0; i < nums.length; i++) {
        if (map.has(nums[i])) {
            res.push(nums[i])
        }
        map.set(nums[i])
    }

    return res
}

console.log(findDuplicate([1, 1, 2, 2, 3, 4, 4, 5, 5]))

// only one including

function checkIncluding(arr) {
    let map = new Map()

    for (let i = 0; i < arr.length; i++) {
        map.set(arr[i])
    }

    return map.keys()
}

console.log(
    checkIncluding([
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        10,
        10,
        2,
        2,
        2,
        3,
        3,
        3,
        4,
        4,
        5,
        5,
        6,
        6,
        7,
        7,
        8,
        8,
        9,
    ]),
)
