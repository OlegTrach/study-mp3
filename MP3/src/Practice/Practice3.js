// two sum

var twoSum = function (nums, target) {
    var res = [];

    for (var i = 0; i < nums.length; i++) {
        var tar = target - nums[i];
        for (var j = i + 1; j < nums.length; j++) {
            if (nums[j] === tar) {
                res = [i, j];
            }
        }
    }

    return res;
};

console.log(twoSum([2, 7, 11, 15], 9));
console.log(twoSum([3, 2, 4], 6));

// twoSum better solution

const twoSum = (nums, target) => {
    let map = new Map();
    for (let i = 0; i < nums.length; i++) {
        let value = target - nums[i];
        if (map.has(value)) {
            return [i, map.get(value)];
        }
        map.set(nums[i], i);
        console.log(map);
    }
};

console.log(twoSum([7, 11, 15, 2], 9));

// twoSumm the best solution

var twoSum = function (nums, target) {
    const numHash = {};

    for (let i = 0; i < nums.length; i++) {
        const num = nums[i];
        const complement = target - num;
        const complementIdx = numHash[complement];

        if (complementIdx !== undefined && complementIdx !== i) {
            return [i, complementIdx];
        }

        numHash[num] = i;
    }
};

console.log(twoSum([2, 7, 11, 15], 9));

// 977. Squares of a Sorted Array

var sortedSquares = function (A) {
    const res = A.map((el) => Math.pow(el));
    console.log(res);

    return res.sort((a, b) => a - b);
};

console.log(sortedSquares([-4, -1, 0, 3, 10]));
// valid paratheleses

var isValid = function (s) {
    const arr = s.split('');
    let res = [arr[0]];

    for (let i = 1; i < arr.length; i++) {
        let current = res[res.length - 1];

        if (current === '{') {
            if (arr[i] === '}') {
                res.pop();
            } else {
                res.push(arr[i]);
            }
        } else if (current === '(') {
            if (arr[i] === ')') {
                res.pop();
            } else {
                res.push(arr[i]);
            }
        } else if (current === '[') {
            if (arr[i] === ']') {
                res.pop();
            } else {
                res.push(arr[i]);
            }
        }
    }

    return res.length === 0;
};

console.log(isValid('{[}]'));

// calculate characters
function calkAmount(arr) {
    const length = arr.length;
    let amount = 1;
    let target = 0;

    for (let i = 0; i < length; i++) {
        if (arr[i] !== arr[i + 1]) {
            arr[target] = arr[i];

            if (amount > 1) {
                arr[target + 1] = amount;
                target += 2;
                amount = 1;
            } else target++;
        } else amount++;
    }

    arr.length = target;

    return arr;
}

console.log('res-1', calkAmount(['a', 'a']));
console.log('res-2', calkAmount(['a', 'a', 'a', 'b', 'c', 'd', 'd', 'd']));
console.log('res-3', calkAmount(['a', 'a', 'b', 'b', 'b', 'c', 'd', 'e']));
console.log('res-4', calkAmount(['a', 'a', 'b', 'c', 'e', 'e']));
console.log('res-5', calkAmount(['a', 'b', 'b', 'c', 'c', 'c', 'd', 'd', 'd']));

// Calculate amounts of items in array!

function calculate(arr) {
    const length = arr.length;
    let amount = 1;
    let amountPos = 0;
    for (let i = 0; i < length; i++) {
        if (arr[i] !== arr[i + 1]) {
            arr[amountPos] = arr[i];
            if (amount > 1) {
                arr[amountPos + 1] = amount;
                amountPos += 2;
                amount = 1;
            } else amountPos++;
        } else amount++;
    }

    arr.length = amountPos;

    return arr;
}

console.log(calculate(['a', 'a', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'e']));
