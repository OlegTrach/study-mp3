// find non double

var singleNumber = function(nums) {
    var sorted = nums.sort((a, b) => a - b)
    console.log(sorted)

    for (var i = 0; i < sorted.length; i += 2) {
        if (sorted[i] != sorted[i + 1]) {
            return sorted[i]
        }
    }
}

console.log(singleNumber([1, 0, 1]))

//  find number position

function findPos(arr, target) {
    for (let i = 0; i < arr.length; i++) {
        if (target <= arr[i]) {
            return i
        }
    }

    if (target > arr[arr.length - 1]) {
        return arr.length
    }
}

console.log(findPos([1, 3, 5, 6], 7))

// last bad version

function firstBadVersion(n) {
    let left = 1
    let right = n

    while (left < right) {
        let mid = left + (left + (right - left)) / 2
        if (isBadVersion(mid)) {
            right = mid
        } else {
            left = mid + 1
        }
    }
    return left
}

// Binary search O(log(n))

let steps = 0

const binarySearch = (array, value) => {
    const length = array.length
    const middle = Math.floor(length / 2)
    const middleElement = array[middle]

    steps++

    if (length === 1) {
        if (array[0] === value) {
            return true
        } else {
            return false
        }
    }

    if (middleElement > value) {
        const leftSide = array.slice(0, middle + 1)
        // console.log('leftSide: ', leftSide)
        return binarySearch(leftSide, value)
    } else if (middleElement < value) {
        const rightSide = array.slice(middle, length)
        // console.log('rightSide: ', rightSide)
        return binarySearch(rightSide, value)
    } else {
        console.log('steps: ' + steps)
        return true
    }
}

console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 7))

const users = [
    {name: 'asd', gender: 'M', groupId: '1'},
    {name: 'asd', gender: 'M', groupId: '1'},
    {name: 'asd', gender: 'M', groupId: '2'},
    {name: 'asd', gender: 'W', groupId: '1'},
]

const groups = [
    {groupId: '1', isDriving: true},
    {groupId: '2', isDriving: false},
]

let driverGroup = groups.filter(g => g.isDriving)[0].groupId

let res = users.filter(
    user => user.gender === 'M' && user.groupId === driverGroup,
)

console.log(res)
console.log(binarySearch([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 7))

// temporrary
class FullTime {
    constructor() {
        this.rate = '$12'
    }
}

class PartTime {
    constructor() {
        this.rate = '$11'
    }
}

class Temporary {
    constructor() {
        this.rate = '$10'
    }
}

class Contractor {
    constructor() {
        this.rate = '$15'
    }
}

class Employee {
    create(type) {
        let employee

        switch (type) {
            case 'fulltime':
                employee = new FullTime()
                break
            case 'parttime':
                employee = new PartTime()
            case 'temporary':
                employee = new Temporary()
            case 'contractor':
                employee = new Contractor()
        }

        employee.type = type

        employee.say = function() {
            console.log(`${this.type}: rate ${this.rate}/hour`)
        }

        return employee
    }
}

const factory = new Employee()
const fulltime = factory.create('fulltime')
const part = factory.create('parttime')

part.say()
fulltime.say()

//  Temporary

Array.prototype.myFlat = function(depth = Infinity) {
    return flatten(this, depth)
}

function flatten(arr, depth) {
    if (depth === 0) {
        return arr
    }

    return arr.reduce((accumulator, el) => {
        if (Array.isArray(el)) {
            accumulator.push(...flatten(el, depth - 1))
        } else {
            accumulator.push(el)
        }

        return accumulator
    }, [])
}

arr = [1, 2, 3, [4, 5, [6, 7, [8, 9, [10, 11, [12, 13, [14, 15]]]]]]]

console.log(arr.flat(0))
console.log(arr.myFlat())

// *********

var arr = [1, 4, 6, 11, 3]

function pearsFun(arr, num) {
    var res = {}

    for (var i = 0; i < arr.length; i++) {
        for (var j = i + 1; j < arr.length; j++) {
            if (arr[i] + arr[j] === num) {
                res[i] = j
            }
        }
    }

    console.log(res)
    return res
}

pearsFun(arr, 7) // {0: 2, 1: 4}

//  ************************

function partition(items, left, right) {
    var pivot = items[Math.floor((right + left) / 2)],
        i = left,
        j = right

    while (i <= j) {
        while (items[i] < pivot) {
            i++
        }
        while (items[j] > pivot) {
            j--
        }
        if (i <= j) {
            swap(items, i, j)
            i++
            j--
        }
    }
    return i
}

function swap(items, firstIndex, secondIndex) {
    const temp = items[firstIndex]
    items[firstIndex] = items[secondIndex]
    items[secondIndex] = temp
}

function quickSort(items, left, right) {
    var index
    if (items.length > 1) {
        index = partition(items, left, right)
        if (left < index - 1) {
            quickSort(items, left, index - 1)
        }
        if (index < right) {
            quickSort(items, index, right)
        }
    }
    return items
}

var items = [5, 8, 9, 7, 10, 4, 2, 1]
var result = quickSort(items, 0, items.length - 1)

console.log(result)

// *******************************

const nums = [1, 5, 8, 3, 5, 77, 2, -2]

function searchPears(number, arr) {
    return arr.reduce((acum, current, index, arr) => {
        let numberToFind = number - current
        let findedIndex = arr.indexOf(numberToFind)

        if (findedIndex !== -1) {
            acum[index] = findedIndex
        }

        return acum
    }, {})
}

console.log(nums)
console.log(searchPears(-1, nums))

//  ********************

let myPromise = new Promise((resolve, reject) => {
    // console.log('I am running!');
    // setTimeout(resolve, 200);
    resolve()
    // reject();
})

// console.log(myPromise);
// console.log('Last line of the programm!');

console.log('1')
setTimeout(() => console.log('1.5'), 0)
myPromise.then(() => {
    console.log('Promise finished!')
})
console.log('3')

requestAnimationFrame(() => console.log('4'))
requestIdleCallback(() => console.log('5'))
console.log('6')

// Matrix rotation

const matrix = [
    ['a1', 'a2', 'a3'], // ['c1', 'b1', 'a1'],
    ['b1', 'b2', 'b3'], //  => ['c2', 'b2', 'a2'],
    ['c1', 'c2', 'c3'], // ['c3', 'b3', 'a3']
]

function rotation(m) {
    const arr = JSON.parse(JSON.stringify(m))
    let l = arr.length - 1

    for (let i = 0; i <= l; i++) {
        for (let j = 0; j <= l; j++) {
            arr[i][j] = m[l - j][i]
        }
    }
    return arr
}

console.log(rotation(matrix))

//  nearly sum return array[sumIndex1, sumIndex2]

const arr = [1, 2, 4, 6, 10, 12]
const target = 15

function nearlySum(a, t) {
    for (let i = 0; i < a.length; i++) {}
}

//  like above example
// TODO implement functionality
function pairwise(arr, tar) {
    // const res = [];
    const res = {}

    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            // if (arr[i] + arr[j] == t) {
            // return [i, j];
            // }
            let currentSum = arr[i] + arr[j]

            if (currentSum == tar) {
                res.firstIndex = arr[i]
                res.secondIndex = arr[j]
                res.sum = arr[i] + arr[j]
                console.log('cursum == tar')

                return [res.firstIndex, res.secondIndex]
            }

            res.sum = res.sum ? res.sum : currentSum

            if (currentSum > tar && currentSum - tar < res.sum) {
                console.log('cursum > tar')
                res.firstIndex = arr[i]
                res.secondIndex = arr[j]
                res.sum = arr[i] + arr[j]
                continue
            }

            if (currentSum < tar && tar - currentSum < res.sum) {
                console.log('cursum < tar')
                res.firstIndex = arr[i]
                res.secondIndex = arr[j]
                res.sum = arr[i] + arr[j]
            }
        }
    }

    return [res.firstIndex, res.secondIndex]
}

console.log(pairwise([1, 41, 10, 2, 0, 5], 8)) // => [3, 5]
// leetcode

var numJewelsInStones = function(J, S) {
    var res = 0
    for (var i = 0; i < J.length; i++) {
        for (var j = 0; j < S.length; j++) {
            if (J[i] === S[j]) {
                res++
            }
        }
    }
    return res
}

console.log(numJewelsInStones('aA', 'aAAbbbb'))
console.log(numJewelsInStones('z', 'ZZ'))

//  another one

var rotateString = function(A, B) {
    // var pos1 = B.indexOf(A[0]);
    // var s = B.slice(pos1) + B.slice(0, pos1);

    // return A === s;
    for (var i = 0; i < A.length; i++) {
        if (A === B.slice(i) + B.slice(0, i)) {
            return true
        }
    }

    return false
}

console.log(rotateString('abcde', 'cdeab'))
console.log(rotateString('abcde', 'abced'))

//

var majorityElement = function(nums) {
    var map = new Map()
    var l = nums.length
    var res = []
    var el

    for (var i = 0; i < l; i++) {
        el = nums[i]
        if (map.has(el)) {
            map.set(el, map.get(el) + 1)
        } else {
            map.set(el, 1)
        }

        if (map.get(el) > l / 3 && res.indexOf(el) < 0) {
            res.push(el)
        }
    }

    return res
}

console.log(majorityElement([3, 2, 3]))
console.log(majorityElement([1, 1, 1, 3, 3, 2, 2, 2]))

// Another one anagrama
function anagram(s1, s2) {
    return (
        s1
            .split('')
            .sort()
            .join('') ===
        s2
            .split('')
            .sort()
            .join('')
    )
}

console.log(anagram('dell', 'lleD'))

//  N-Repeated Element in Size 2N Array

var repeatedNTimes = function(A) {
    let res = {}
    for (let i = 0; i < A.length; i++) {
        if (res[A[i]]) {
            res[A[i]] = res[A[i]] + 1
        } else {
            res[A[i]] = 1
        }
    }

    for (key in res) {
        if (res[key] == A.length / 2) {
            return key
        }
    }

    return 'there is no repeted elements'
}

console.log(repeatedNTimes([1, 2, 3, 3]))

var repeatedNTimes = function(A) {
    let map = new Map()
    for (let i = 0; i < A.length; i++) {
        let k = A[i]
        if (map.has(k)) {
            map.set(k, map.get(k) + 1)
        } else {
            map.set(k, 1)
        }
    }

    for (let key of map.keys()) {
        if (map.get(key) == A.length / 2) {
            return key
        }
    }

    return 'there is no repeted elements'
}

console.log(repeatedNTimes([1, 2, 3, 3]))

// Flipping an Image

function rotation(m) {
    const arr = JSON.parse(JSON.stringify(m))
    let l = arr.length - 1

    for (let i = 0; i <= l; i++) {
        for (let j = 0; j <= l; j++) {
            arr[i][j] = m[i][l - j] == 1 ? 0 : 1
        }
    }
    return arr
}

console.log(rotation([[1, 1, 0], [1, 0, 1], [0, 0, 0]]))

// flip image

var flipAndInvertImage = function(A) {
    var B = A.map(x => {
        var list = x.reverse()
        return list.map(i => 1 - i)
    })
    return B
}

console.log(flipAndInvertImage([[1, 1, 0], [1, 0, 1], [0, 0, 0]]))

// test

const matrix = [[1, 1, 0], [1, 0, 1], [0, 0, 0]]

function rotation(m) {
    const arr = JSON.parse(JSON.stringify(m))
    let l = arr.length - 1

    for (let i = 0; i <= l; i++) {
        for (let j = 0; j <= l; j++) {
            arr[i][j] = m[l - j][i]
        }
    }
    return arr
}

console.log(rotation(matrix))

// improvment for rotation matrix

var flipAndInvertImage = function(A) {
    var B = A.map((x, index) => {
        let list = x[index]
        return x[index]
        // return list.map((i) => 1 - i);
    })
    return B
}

console.log(flipAndInvertImage([[1, 1, 0], [1, 0, 1], [0, 0, 0]]))

// rectangle

var isRectangleOverlap = function(rec1, rec2) {
    return rec1[2] > rec2[0] && rec2[1] < rec1[3]
}

console.log(isRectangleOverlap([0, 0, 1, 1], [1, 0, 2, 1]))

// Range Sum of BST

var rangeSumBST = function(root, L, R) {
    var res = 0

    if (R === undefined) {
        for (var i = 0; i < root.length; i++) {
            if (root[i] >= L) {
                res += root[i]
            }
        }
    } else {
        for (var i = 0; i < root.length; i++) {
            if (root[i] <= R && root[i] >= L) {
                res += root[i]
            }
        }
    }

    return res
}

console.log(rangeSumBST([10, 5, 15, 3, 7, null, 18], 7, 15))
console.log(rangeSumBST([10, 5, 15, 3, 7, 13, 18, 1, null, 6], 6, 10))
console.log(rangeSumBST([10, 5, 15, 3, 7, null, 18], 7))

// find odd numbers of array
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

console.log(arr.find(el => el % 2 === 0))
console.log(arr.filter(el => el % 2 === 0))

// const arr1 = [[1,]]

// const test = Array.from([1, "b", "v"]);
// const test1 = Array.of(1, 2, "3", 4, 5);

// console.log(test, test1);
