// POLYFILL BIND
Function.prototype.bind = function () {
    var fn = this,
        args = Array.prototype.slice.call(arguments),
        object = args.shift();
    return function() {
        return fn.apply(object, args.concat(Array.prototype.slice.call(arguments)));
    };
};

var myObject = {
    x: "My value"
};

var foo = function() {
    console.log(this.x);
}.bind(myObject);

foo();

// POLYFILL forEach

Array.prototype.myForEach = function(callback) {
    for(var i = 0; i < this.length; i++)
        callback(this[i], i, this);
};

var arr = ["one", "two", "three", "four", "five"];
arr.myForEach(function(word) {
    console.log("word", word);
})

// POLYFILL MAP

Array.prototype.myMap = function(callback) {
    arr = [];
    for(var i = 0; i < this.length; i++)
        arr.push(callback(this[i], i, this));
    return arr;
};

var arrs = ["Mum", "Dad", "Grandpa", "Grandma"];
var numbers1 = [1, 2, 3, 4, 5];

var relatives = arrs.myMap(function(n){
    return n;
});

var doubles = numbers1.myMap(function(num){
    return num * num;
});

console.log(relatives);
console.log(doubles);

// PLYFILL FILTER

Array.prototype.myFilter = function(callback, context){
    arr = [];
    for(var i = 0; i < this.length; i++) {
        if(callback.call(context, this[i], i, this))
            arr.push(this[i]);
    }
    return arr;
};

var numbers2 = [1, 20, 30, 80, 2, 9, 3];
var newNum = numbers2.myFilter(function(n){
    return n >= 10;
});
console.log(newNum);

// POLYFILL REDUCE

Array.prototype.myReduce = function(callback, initialVal) {
    var accumulator = (initialVal === undefined) ? undefined : initialVal;
    for (var i = 0; i < this.length; i++) {
        if (accumulator !== undefined) {
            accumulator = callback.call(undefined, accumulator, this[i], i, this);
        } else 
            accumulator = this[i];
    }

    return accumulator;
};

var numbers3 = [20, 20, 2, 3];
var total = numbers3.myReduce(function(a, b){
    return a + b;
}, 10);

console.log(total);

var flattened = [
    [0,1],
    [2,3],
    [4,5]
].reduce(function(a, b){
    return a.concat(b);
});
console.log(flattened); 

// POLYFILL EVERY 

Array.prototype.myEvery = function(callback, context) {
    for(var i = 0; i < this.length; i++){
        if(!callback.call(context, this[i], i, this))
            return false;
    }
    return true;
};

var passed = [12, 5, 8, 130, 44].myEvery(function(element) {
    return (element >= 10);
});
console.log(passed);


// POLYFILL SOME

Array.prototype.mySome = function(callback, context) {
    for(var i = 0; i < this.length; i++){
        if(callback.call(context, this[i], i, this))
            return true;
    }
    return false;
};

var passed1 = [12, 5, 8, 130, 44].mySome(function(element){
    return (element >= 200);
});

console.log(passed1);

// POLYFIL INCLUDES

Array.prototype.myIncludes = function(element, startIndex) {
    if (!this.length || (startIndex && this.length < startIndex)) return;

    for(var i = startIndex || 0; i < this.length; i++) {
        if(element === this[i]) {
            return true;
        }
    }
    return false;
}

Array.prototype.myIncludesWithCallback = function(callback, startIndex, context){
	for(var i = startIndex || 0; i < this.length; i++){
		if(callback.call(context, this[i], i, this)){
			return true;
		} 
	}
	return false;
};

var arr = [1, 2, 3, 4, 5];

var res = arr.myIncludesWithCallback(function(element){
	return (element  === 3);
});

console.log(res);

// PROMISES
const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        if(true) {
            resolve("Hello from promise")
        } else {
            reject()
        }
    }, 1)
})

promise
    // .then(data => console.log("success: ", data)) // first variant
    // .catch(error => console.log("error: ", error));

    .then(
        // data => console.log("success: ", data), // second variant, change tine in timeout
        // error => console.log("error: ", error)
    ) 

// ASYNC/AWAIT
const fetchProducts = () => Promise.resolve({data: [1, 2, 3]});
const fetchAdditional = () => Promise.resolve({data: [2, 3, 4]})

const getProducts = async () => {
    const products = await fetchProducts();

    if(!products.data.length) {
        return products.data
    }

    return await fetchAdditional(products.data)
}

getProducts().then(result => {
    console.log("result - ", result);
});

// ASYNC/AWAIT second
function getUser(id) {
    return Promise.resolve({ id : 1 });
}

async function main() {
    let user = await getUser(1); // await ставиться перед будь якою функцією що повертає promise
    console.log(user);
}

// main();

// PATTERNS

// ***CONSTRUCTOR***
function Person(name, age, country) {
    this.name = name;
    this.age = age;
    this.country = country;
}

const person1 = new Person("John", 23, "UA");
// console.log("Person", person1);

// ***SINGLETON***
var counterModule = (function () {
    var counter,
        instance;

    var getCounter = function () {
        return counter;
    }

    var increaseCounter = function () {
        counter++;
    }

    var createInstance = function () {
        return {
            getCounter: getCounter,
                increaseCounter: increaseCounter
            }
        }

    return {
        getInstance: function () {
            return instance || (instance = createInstance());
        }
    }
}());

// console.log("counterModule", counterModule.getInstance());

// ***FACTORY***
class FullTime {
    constructor() {
        this.rate = "$12"
    }
}

class PartTime {
    constructor() {
        this.rate = "$11"
    }
}

class Temporary {
    constructor() {
        this.rate = "$10"
    }
}

class Contractor {
    constructor() {
        this.rate = "$15"
    }
}

class Employee {
    create(type) {
        let employee;
        if (type == "fulltime"){
            employee = new FullTime() 
        } else if (type === "parttime") {
            employee = new PartTime()
        } else if ( type  === "temporary") {
            employee = new Temporary()
        } else if ( type  === "contractor") {
        employee = new Contractor()
        }
        employee.type = type;
        employee.say = function() {
            console.log(`${this.type}: rate ${this.rate}/hour`)
        }
        return employee
    }
}

const factory = new Employee();
fulltime = factory.create("fulltime");
parttime = factory.create("parttime");
temporary = factory.create("temporary");
contractor = factory.create("contractor");

// fulltime.say();
// parttime.say();
// temporary.say();
// contractor.say();

//  ***MODULE***
var counterModule = (function(){
    var counter = 0;

    var increaseCounter = function() {
        counter++;
    }

    var getCounter = function() {
        return counter;
    }

    return {
        getCounter: getCounter,
        increaseCounter: increaseCounter
    }
}())

// counterModule.increaseCounter();
// counterModule.increaseCounter();
// counterModule.increaseCounter();
// console.log(counterModule.getCounter());
