// Liskov substitution principle

// Необхідно щоб підкласи могли служити заміною суперкласів
// Іншими словами
// Функції що використовують базовий тип повинні мати можоивість використовувати його підтипи
// Основна ціль проектувати логіку таким чином щоб класи наслідники могли спокійно використовуватися щамість батьків

// Explanation 1
class Rectangle {
    constructor(width, height) {}
    seWidth(width) {
        this.width = width;
    }

    setHeight(height) {
        this.height = height;
    }

    areaOf() {
        return this.width * this.height;
    }
}

class Square extends Rectangle {
    width = 0;
    height = 0;

    constructor(size) {
        super(size, size);
    }

    seWidth(width) {
        this.width = width;
        this.height = width;
    }

    setHeight(height) {
        this.width = height;
        this.height = height;
    }
}
// we should overwrite methods
const mySquare = new Square(20);
mySquare.seWidth(30);
mySquare.setWidth(40);

// And now we have an issue when we change width we change height as well
//How it should works
const changeShapeSize = (figure) => {
    figure.seWidth(10); // width is 10, height is 0
    figure.seHeight(20); // width is 10, height is 20
}
// How it works
const changeShapeSize = (figure) => {
    figure.setWidth(10); // width is 10, height is 10
    figure.setHeight(20); // width is 20, height is 20
}

// So we nee dto create common interface TypeScript
class Figure { // interface
    setWidth(width) {}
    setHeight(height) {}
    areaOf() {}
}

class Rectangle extends Figure { // implements
    setWidth(width) {}
    setHeight(height) {}
    areaOf() {}
}

class Square extends Figure { // implements
    setWidth(width) {}
    setHeight(height) {}
    areaOf() {}
}

// Explanation 2
class Person {}
class Member extends Person {
    access() {
        console.log('You have access')
    }
}

class Guest extends Person {
    isQuest = true;
}

class Frontend extends Member {
    canCreateFrontend() {

    }
}

class Backend extends Member {
    canCreateBackend() {

    }
}

class PersonFromDifferentCompany extends Guest {
    access() {
        throw new Error('You do not have access');
    }
}

function openSecretDoor(person) {
    person.access();
}

openSecretDoor(new Frontend());
openSecretDoor(new Backend());
// openSecretDoor(new PersonFromDifferentCompany()); // There should be member


// SECOND REALIZATION

// class Component {
//     // Base functionality
//     isComponent = true;
// }

// class ComponentWithTemplate extends Component {
//     render() {
//         return `<div>Component</div>`
//     }
// }

// class HigherOrderComponent extends Component {}

// class HeaderComponent extends ComponentWithTemplate {
//     onInit() {}
// }

// class FooterComponent extends ComponentWithTemplate {
//     afterInit() {}
// }

// class HOC extends HigherOrderComponent { // HOC should not have render method
//     render() {
//         throw new Error('Render is impossible here');
//     }

//     wrapComponent(component) {
//         component.wrapped = true;
//         return component;
//     }
// }

// function renderComponent(component) {
//     console.log(component.render());
// }

// renderComponent(new HeaderComponent());
// renderComponent(new FooterComponent());
// // renderComponent(new HOC());