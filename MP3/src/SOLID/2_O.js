// Open Close Principle

// Classes should be open for extensions and closed for modifications
// Модуль повинен відчинений для розширення але зачинений для змін
// Основна мета розробка стійкого до змін програми

// Explanation 1
class Auto {
    constructor(model) {}
    getCarModel() {}
}

const shop = [
    new Auto('Tesla'),
    new Auto('Audi')
]

const getPrice = (auto) => {
    for (let i =0; i < auto.length; i++) {
        switch (auto[i].model) {
            case 'Tesla': return '80 000$';
            case 'Audi': return '50 000$';
            default: 'No Auto Price';
        }
    }
}

getPrice(shop);

// if we will add BMW we should rewrite function getPrice
// we should use our new principle here
// lets set some abstract class
class carPrice {
    getPrice()
}

class Tesla extends carPrice {
    getPrice() {
        return '80 000$';
    }
}

class Audi extends carPrice {
    getPrice() {
        return '50 000$';
    }
}

class BMW extends carPrice {
    getPrice() {
        return '70 000$';
    }
}

// now we modify our logic
const shop = [
    new Tesla(),
    new Audi(),
    new BMW()
];

const getPrice = (auto) => {
    for (let i = 0; i < auto.length; i++) {
        auto[i].getPrice();
    }
}

getPrice(shop);

// Explanation 2
class Shape { // Base class
    area() {
        throw new Error('Area method should be implemented');
    }
}

class Square extends Shape {
    constructor(size) {
        super();
        this.size = size;
    }

    area() {
        return this.size **2
    }
}

class Circle extends Shape {
    constructor(radius) {
        super();
        this.radius = radius;
    }

    area() {
        return (this.radius ** 2) * Math.PI;
    }
}

class Rect extends Shape {
    constructor(width, height) {
        super();
        this.width = width;
        this.height = height;

    }

    area() {
        return this.width * this.height;
    }
}

class Triangle extends Shape {
    constructor(a, b) {
        super();
        this.a = a;
        this.b = b;
    }
    
    area() {
        return (this.a * this.b) / 2;
    }
}

// calculate Area

class AreaCalculator {
    constructor(shapes = []) {
        this.shapes = shapes;
    }

    sum() {
        return this.shapes.reduce((acc, shape) => {
            return acc+= shape.area();
        }, 0)
    }
}

const calc = new AreaCalculator([
    new Square(10),
    new Circle(1),
    new Circle(5),
    new Rect(10, 20),
    new Triangle(10, 15)
])

console.log(calc.sum());