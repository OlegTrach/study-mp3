// Interface segregation principle

// Сутності не повинні залежати від інтерфейсів які вони використовують
// Основна проблема ООП що при створення наслідник може отримати купу методів при наслідування кі йому непотрібні

// Explanation 1
class AutoSet { // interface
    getTeslaSet();
    getAudiSet();
    getBMWSet();
}

// Now when we inherit from this interface we get
// and we should overwrite every method from interface

class Tesla extends  AutoSet { // implements
    getTeslaSet() {};
    getAudiSet() {};
    getBMWSet() {};
}

class Audi extends AutoSet { // implements
    getTeslaSet() {};
    getAudiSet() {};
    getBMWSet() {};
}

class BMW extends AutoSet { // implements
    getTeslaSet() {};
    getAudiSet() {};
    getBMWSet() {};
}

// for reduce redundant methods we should split interfaces

class TeslaSet { // interface
    getTeslaSet()
}

class AudiSet { // interface
    getAudiSet()
}

class BMWSet { // interface
    getBMWSet()
}

class Tesla extends TeslaSet { // implements
    getTeslaSet() {}
}

class Audi extends AudiSet { // implements
    getAudiSet() {}
}

class BMW extends BMWSet { // implements
    getBMWSet() {}
}

// Explanation 2

// class Animal {
//     constructor(name) {
//         this.name = name;
//     }

//     walk() {
//         console.log(`${this.name} can walk!`);
//     }

//     swim() {
//         console.log(`${this.name} can swim`);
//     }

//     fly() {
//         console.log(`${this.name} can fly!`);
//     }
// }

// class Dog extends Animal {
//     fly() { // Overwrite parent method
//         return null;
//     }
// }

// class Eagle extends Animal {
//     swim() {
//         return null;
//     }
// }

// class Whale extends Animal {
//     fly() {
//         return null;
//     }

//     walk() {
//         return null;
//     }
// }

// const dog = new Dog('Rex');

// dog.walk();
// dog.swim();
// dog.fly(); // call own method

// const eagle = new Eagle('Eagleee');

// eagle.fly();
// eagle.swim();// call own method
// eagle.walk();

// const whale = new Whale('Big friend');
// whale.swim();
// whale.walk();
// whale.fly();

// we can solve previous task using composition API

class Animal {
    constructor(name) {
        this.name = name;
    }
}

const swimmer = {
    swim() {
        console.log(`${this.name} can swim`);
    }
}

const flier = {
    fly() {
        console.log(`${this.name} can fly`);
    }
}

const walker = {
    walk() {
        console.log(`${this.name} can walk`);
    }
}

class Dog extends Animal {}
class Eagle extends Animal {}
class Whale extends Animal {}

Object.assign(Dog.prototype, swimmer, walker);
Object.assign(Eagle.prototype, flier, walker);
Object.assign(Whale.prototype, swimmer);

const dog = new Dog('Rex');
dog.walk();
dog.swim();

const eagle = new Eagle('Eagleee');
eagle.fly();
eagle.walk();

const whale = new Whale('Big friend');
whale.swim();
