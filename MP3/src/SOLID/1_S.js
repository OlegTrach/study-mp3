// Single Responsibility Principle

// Any class could have only one zone of responsibility
// В Модуля повинна бути тільки одна причина для зміни або Клас повинен відповідати ітльки за щось одне
// Мета оргінізувати свій код таким чином щоб у випадку зміни залучити щонайменше модулів
//DECOMPOSITION

// Explanation 1
class Auto {
    constructor(model) {}
    getCarModel() {}
    saveCustomerOrder() {}
}

// Our class after ome month
class Auto {
    constructor(model) {}
    getCarModel() {}
    saveCustomerOrder(o) {}
    setCarModel() {}
    getCustomerOrder(id) {}
    removeCustomerOrder(id) {}
    updateCarSet(set) {}
}

// Lets split it to different entities
class Auto { // Interaction with auto
    constructor(model) {}
    getCarModel() {}
    setCarModel() {}
}

class CustomerAuto { // Work with client orders
    saveCustomerOrder(o) {}
    getCustomerOrder(id) {}
    removeCustomerOrder(id) {}
}

class AutoDB { // Manipulation in DB
    updateCarSet(set) {}
}


// Explanation 2
class News {
    constructor(title, text) {
        this.title = title;
        this.text = text;
        this.modified = false;
    }

    update(text) {
        this.text = text;
        this.modified = true;
    }

    // It is not correct to have two methods with similar functionality here so we need to rewrite!

    // toHTML() {
    //     return `
    //         <div class="news">
    //             <h1>${this.title}</h1>
    //             <p>${this.text}</p>
    //         </div>
    //     `
    // }

    // toJSON() {
    //     return JSON.stringify({
    //         title: this.title,
    //         text: this.text,
    //         modified: this.modified
    //     }, null, 4)
    // }
}

// Thats why we need a new class for single responsibility

class NewsPrinter {
    constructor(news) {
        this.news = news;
    }

    html() {
        return `
            <div class="news">
                <h1>${this.news.title}</h1>
                <p>${this.news.text}</p>
            </div>
        `
    }

    json() {
        return JSON.stringify({
                    title: this.news.title,
                    text: this.news.text,
                    modified: this.news.modified
                }, null, 4)
    }

    xml() {
        return `
            <news>
                <title>${this.news.title}</title>
                <text>${this.news.text}</text>
            </news>
        `
    }
}

const news = new News('Name', 'News text');

console.log(news);
// console.log(news.toHTML());
// console.log(news.toJSON());

// now we can print

const printer = new NewsPrinter(news);

console.log(printer.html());
console.log(printer.json())
console.log(printer.xml());
