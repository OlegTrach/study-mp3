// Dependency inversion principle

// Levels should be depend on abstraction
// Модулі вищих рівнів не повинні залежати від модулів нижніх рівнів
// Обидвоє повинні залежати від абстракцій
// Абстракції не повинні залежати від деталей
// Деталі повинні залежати від абстракцій

// Explanation 1
class xmlHttpRequestService { }

// low level
class xmlHttpService extends xmlHttpRequestService {
    request(url, type){ }
}

// High level module
class Http {
    constructor(xmlHttpService) {}

    get(url, options) {
        this.xmlHttpService.request(url, 'GET');
    }

    post(url) {
        this.xmlHttpService.request(url, 'POST')
    }
}

// we should create interface

class Connection { // interface
    request(url, options)
}

// now we pass interface to high level class
class Http {
    constructor(httpConnection) {}

    get(url, options) {
        this.httpConnection.request(url, 'GET');
    }

    post(url) {
        this.httpConnection.request(url, 'POST');
    }
}

// now we should rewrite class xmlHttprequestService
class xmlHttpRequestService {
    open() {}
    send() {}
}

class Connection { // interface
    request(url, options)
}

class xmlHttpService extends Connections { // implements
    xhr = new xmlHttpRequestService();

    request(url, type) {
        this.xhr.open();
        this.xhr.send();
    }
}
// Explanation 2

class Fetch {
    request(url) {
        // return fetch(url).then(r => r.json());
        return Promise.resolve('data from fetch');
    }
}

class LocalStorage {
    get() {
        // return localStorage.getItem('key')
        return 'Data from localStorage';
    }
}

class FetchClient {
    constructor() {
        this.fetch = new Fetch();
    }

    clientGet() {
        return this.fetch.request('google.com');
    }
}

class LocalStorageClient {
    constructor() {
        this.localStorage = new LocalStorage();
    }

    clientGet(key) {
        return this.localStorage.get(key);
    }
}

class Database {
    constructor(client) {
        this.client = client;
    }

    getData(key) {
        return this.client.clientGet(key);
    }
}

const db1 = new Database(new FetchClient('ukr.net'));
const db2 = new Database(new LocalStorageClient());
 
db1.getData('rand').then(res => console.log(res));
console.log(db2.getData('rand'));