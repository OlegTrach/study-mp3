const obj = {
    bar: 1,
    foo() {
        return this.bar
    },
    baz: () => this.bar
};

const a = obj.foo;

console.log(a());
console.log(a.bind(obj)());
console.log(obj.baz());

// ---------

function test() {
    return () => this.foo;
}

console.log(test.call({ foo: 1 })());

// ---------

const obj1 = {
    foo: 1,
    obj2: {
        foo: 2,
        baz: () => this.foo
    }
}

var foo = 25;

console.log(obj1.obj2.baz());

// -----------

const arr = [1, 2, 3, 4, 5];
const Obj = { name: 'Oleh', surname: 'Trach', age: 37 };

for (let a of arr) {
    console.log(a);
}

for (let a in Obj) {
    console.log(a);
}
